Yoi-Web
===
An experimental framework for simplifying some portions of the WebGPU api and making 
it a bit more streamlined to use. 

Running
===
* `cargo run` <crate name>
* `cargo run-wasm`<crate name>
* Note that at the moment, on the web, the WGSL syntax used is the latest spec. For whatever reason though, that same syntax is not usable on the desktop. 