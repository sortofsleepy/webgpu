use glam::{vec2, Vec3, vec4, Vec4};
use crate::traits::Geometry;

#[derive(Copy, Clone)]
pub struct RoomSettings {
    eye_dir_power: Vec4,
    room_dimensions: Vec4,
    color_light_power: Vec4,
}
/// Room object from @flight404's Eyeo 2012 talk
/// https://github.com/flight404/Eyeo2012
pub struct Room {}

impl Room {
    pub fn generate() -> Geometry {
        let x = 1.0 as f32;
        let y = 1.0 as f32;
        let z = 1.0 as f32;

        let verts = vec![
            vec4(-x, -y, -z, 1.0), vec4(-x, -y, z, 1.0),
            vec4(x, -y, z, 1.0), vec4(x, -y, -z, 1.0),
            vec4(-x, y, -z, 1.0), vec4(-x, y, z, 1.0),
            vec4(x, y, z, 1.0), vec4(x, y, -z, 1.0),
        ];

        let v_indices = vec![
            [0, 1, 3], [1, 2, 3],    // floor
            [4, 7, 5], [7, 6, 5],    // ceiling
            [0, 4, 1], [4, 5, 1],    // left
            [2, 6, 3], [6, 7, 3],    // right
            [1, 5, 2], [5, 6, 2],    // back
            [3, 7, 0], [7, 4, 0],  // front
        ];

        let v_normals = vec![
            vec4(0.0, -1.0, 1.0, 1.0),    // floor
            vec4(0.0, -1.0, 1.0, 1.0),    // ceiling
            vec4(-1.0, 0.0, 3.0, 1.0),    // left
            vec4(-1.0, 0.0, 3.0, 1.0),    // right
            vec4(0.0, 1.0, -1.0, 1.0),    // back
            vec4(0.0, 0.0, 1.0, 1.0),    // front
        ];

        let v_tex_coords = vec![
            vec2(0.0, 0.0),
            vec2(0.0, 1.0),
            vec2(1.0, 1.0),
            vec2(1.0, 0.0),
        ];

        let t_indices = vec![
            [0, 1, 3], [1, 2, 3],    // floor
            [0, 1, 3], [1, 2, 3],    // ceiling
            [0, 1, 3], [1, 2, 3],    // left
            [0, 1, 3], [1, 2, 3],    // right
            [0, 1, 3], [1, 2, 3],    // back
            [0, 1, 3], [1, 2, 3],    // front
        ];

        let mut positions = vec![];
        let mut indices = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        for i in 0..12 {
            positions.push(verts[v_indices[i][0]]);
            positions.push(verts[v_indices[i][1]]);
            positions.push(verts[v_indices[i][2]]);


            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);

            uvs.push(v_tex_coords[t_indices[i][0]]);
            uvs.push(v_tex_coords[t_indices[i][1]]);
            uvs.push(v_tex_coords[t_indices[i][2]]);
        }

        for i in 0..36 {
            indices.push(i);
        }

        Geometry {
            positions,
            normals,
            uvs,
            indices,
        }
    }
}

// Default vertex shader for Room geometry.
pub const ROOM_VERTEX: &str = "#version 450

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;


layout(binding = 1) uniform RoomSettings{

    vec4 eye_dir_power;
    vec4 room_dimensions;
    vec4 color_light_power;
} room;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;


layout(location = 0) out vec4 vNormal;
layout(location = 1) out vec3 vEyeDir;
layout(location = 2) out vec3 vPos;


void main()
{
    vec4 pos = position;
    pos.xyz *= room.room_dimensions.xyz;

    vPos = pos.xyz;
    vNormal = normal;
    vEyeDir = normalize(room.eye_dir_power.xyz - position.xyz);
    gl_Position = ubo.proj * ubo.view * vec4(pos.xyz,1.);

}";

// Default fragment shader to use for room geometry.
pub const ROOM_FRAGMENT: &str = "#version 450

layout(location = 0) out vec4 glFragColor;

layout(binding = 1) uniform RoomSettings{

    vec4 eye_dir_power;
    vec4 room_dimensions;
    vec4 color_light_power;
} room;

layout(location = 0) in vec4 vNormal;
layout(location = 1) in vec3 vEyeDir;
layout(location = 2) in vec3 vPos;

void main(){
    float aoLight = 1.0 - length( vPos ) * ( 0.0015 + ( room.eye_dir_power.a * 0.005 ) );
    float ceiling		= 0.0;
    if( vNormal.y < -0.5 ) ceiling = 1.0;

    float yPer = clamp( -vPos.y / room.room_dimensions.y, 0.0, 1.0 );
    float ceilingGlow	= pow( yPer, 2.0 ) * 0.25;
    ceilingGlow			+= pow( yPer, 200.0 );
    ceilingGlow			+= pow( max( yPer - 0.7, 0.0 ), 3.0 ) * 4.0;
    vec3 finalColor		= vec3( aoLight + ( ceiling + ceilingGlow * 0.5 ) * room.color_light_power.a );

    glFragColor = vec4(finalColor * vec3(1.0,1.0,0.0),1.);
}";