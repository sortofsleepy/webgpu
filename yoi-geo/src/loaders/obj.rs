use tobj;
use std::fs;

/// OBJ helpers using https://github.com/Twinklebear/tobj

/// Loads obj model. Parses values out to Vec objects of data.
/// Currently does simplified loading, see tobj crate for more examples.
pub fn load_model(filepath: &str) {
    let s = format!("Something went wrong trying to load .obj model at {}", filepath);
    let model = fs::read_to_string(filepath).expect(s.as_str());

    let (models, materials) = tobj::load_obj(
        &model.as_str(),
        &tobj::LoadOptions::default(),
    ).expect("failed to parse obj");

    for (i, m) in models.iter().enumerate() {
        let mesh = &m.mesh;

        if cfg!(debug_assertions) {
            println!("");
            println!("model[{}].name             = \'{}\'", i, m.name);
            println!("model[{}].mesh.material_id = {:?}", i, mesh.material_id);
            println!(
                "model[{}].face_count       = {}",
                i,
                mesh.face_arities.len()
            );
        }


        let mut next_face = 0;
        for face in 0..mesh.face_arities.len(){

        }
    }
}