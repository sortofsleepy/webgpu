use glam::{vec2, vec4};
use crate::traits::Geometry;

pub struct PlaneGeometry {
    pub geo: Geometry,
}

impl PlaneGeometry {
    pub fn create(width: f32, height: f32, width_segments: f32, height_segments: f32) -> Geometry {
        let width_half = width / 2.0;
        let height_half = height / 2.0;

        let grid_x = width_segments.floor();
        let grid_y = height_segments.floor();

        let grid_x1 = grid_x + 1.0;
        let grid_y1 = grid_y + 1.0;

        let segment_width = width / grid_x;
        let segment_height = height / grid_y;
        let mut vertices = vec![];
        let mut uvs = vec![];

        let mut sizey = grid_y1 as u32;
        let mut sizex = grid_x1 as u32;

        for iy in 0..sizey {
            let y = (iy as f32) * segment_height - height_half;

            for ix in 0..sizex {
                let x = (ix as f32) * segment_width - width_half;
                vertices.push(vec4(x, -y, 0.0, 1.0));

                uvs.push(vec2(
                    (ix as f32) / grid_x,
                    (iy as f32) / grid_y,
                ));
            }
        }

        sizey = grid_y as u32;
        sizex = grid_x as u32;

        let mut indices: Vec<u32> = Vec::new();
        for iy in 0..sizey {
            for ix in 0..sizex {
                let a = ix + (grid_x1 as u32) * iy;
                let b = ix + (grid_x1 as u32) * (iy + 1);
                let c = ix + 1 + (grid_x1 as u32) * (iy + 1);
                let d = ix + 1 + (grid_x1 as u32) * iy;

                indices.push(a);
                indices.push(b);
                indices.push(d);
                indices.push(b);
                indices.push(c);
                indices.push(d);
            }
        }
        Geometry {
            positions: vertices,
            indices,
            normals: vec![],
            uvs,
        }
    }
}
