use glam::Vec4;

/// Based on Three.js Tessellate modifier.
/// https://github.com/mrdoob/three.js/blob/master/examples/js/modifiers/TessellateModifier.js
pub struct Tesssellate {
    max_edge_length: f32,
    max_iterations: u8,
}

impl Default for Tesssellate {
    fn default() -> Self {
        Tesssellate {
            max_edge_length: 0.1,
            max_iterations: 6,
        }
    }
}

impl Tesssellate {
    pub fn new(max_edge_length: f32, max_iterations: u8) -> Self {
        Tesssellate {
            max_edge_length,
            max_iterations,
        }
    }

    /// Tessellates position vec4 attribute(Assume to be position)
    pub fn modify_positions(&self, positions: &Vec<Vec4>) -> Vec<Vec4> {
        let edge_length_sq = self.max_edge_length * self.max_edge_length;

        let mut va = Vec4::default();
        let mut vb = Vec4::default();
        let mut vc = Vec4::default();
        let mut vm = Vec4::default();

        let vs = [va, vb, vc, vm];

        let mut iteration: usize = 0;
        let mut tesselating = true;

        let mut positions2 = positions.clone();

        let get_triangle_vertices = |a: usize, b: usize, c: usize| -> [Vec4; 3]{
            let v1 = vs[a];
            let v2 = vs[b];
            let v3 = vs[c];

            [v1, v2, v3]
        };

        let mut pos = vec![];

        while tesselating && iteration < self.max_iterations as usize {
            iteration += 1;
            tesselating = false;
            pos = positions2.clone();

            let mut i2: usize = 0;

            for i in (0..pos.len()).step_by(3) {
                va = pos[i];
                vb = pos[i + 1];
                vc = pos[i + 2];

                let dab = va.distance_squared(vb);
                let dbc = vb.distance_squared(vc);
                let dac = va.distance_squared(vc);

                if dab > edge_length_sq || dbc > edge_length_sq || dac > edge_length_sq {
                    tesselating = false;

                    if dab >= dbc && dab >= dac {
                        vm = va.lerp(vb, 0.5);
                        let mut val = get_triangle_vertices(0, 3, 1);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);

                        val = get_triangle_vertices(3, 1, 2);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);
                    } else if dbc >= dab && dbc >= dac {
                        vm = vb.lerp(vc, 0.5);
                        let mut val = get_triangle_vertices(0, 1, 3);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);

                        val = get_triangle_vertices(3, 2, 0);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);
                    } else {
                        vm = va.lerp(vc, 0.5);

                        let mut val = get_triangle_vertices(0, 1, 3);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);

                        val = get_triangle_vertices(3, 1, 2);
                        positions2.push(val[0]);
                        positions2.push(val[1]);
                        positions2.push(val[2]);
                    }
                } else {
                    let val = get_triangle_vertices(0, 1, 2);
                    positions2.push(val[0]);
                    positions2.push(val[1]);
                    positions2.push(val[2]);
                }

                i2 += 6;
            }
        }

        positions2
    }
}