use glam::{Vec2, vec2, Vec4, vec4};
use crate::traits::{Geo, Geometry};

/// port of primitive-cube package by @vorg
/// https://github.com/vorg/primitive-cube/blob/master/index.js
pub struct CubeGeo {
    pub positions: Vec<Vec4>,
    pub normals: Vec<Vec4>,
    pub uvs: Vec<Vec2>,
    pub indices: Vec<u32>,
    vertex_index: i32,
}

impl CubeGeo {
    pub fn new(sx: i32, sy: i32, sz: i32, nx: i32, ny: i32, nz: i32) -> Self {
        let mut cube = CubeGeo {
            positions: vec![],
            normals: vec![],
            uvs: vec![],
            indices: vec![],
            vertex_index: 0,
        };

        cube.make_plane(0, 1, 2, sx, sy, nx, ny, sz / 2, 1, -1); //front
        cube.make_plane(0, 1, 2, sx, sy, nx, ny, -sz / 2, -1, -1); //back
        cube.make_plane(2, 1, 0, sz, sy, nz, ny, -sx / 2, 1, -1); //left
        cube.make_plane(2, 1, 0, sz, sy, nz, ny, sx / 2, -1, -1); //right
        cube.make_plane(0, 2, 1, sx, sz, nx, nz, sy / 2, 1, 1); //top
        cube.make_plane(0, 2, 1, sx, sz, nx, nz, -sy / 2, 1, -1); //bottom

        cube
    }
    pub fn get_raw_geometry(&mut self) -> Geometry {
        Geometry {
            positions: self.positions.clone(),
            normals: self.normals.clone(),
            uvs: self.uvs.clone(),
            indices: self.indices.clone(),
        }
    }
    // TODO figure out normals
    pub fn make_plane(&mut self, u: usize, v: usize, w: usize, su: i32, sv: i32, nu: i32, nv: i32, pw: i32, flipu: i32, flipv: i32) {
        let vert_shift = self.vertex_index;

        for j in 0..nv + 1 {
            for i in 0..nu + 1 {
                let mut vert = Vec4::new(0.0, 0.0, 0.0, 1.0);

                vert[u] = ((-su / 2 + i * su / nu) * flipu) as f32;
                vert[v] = ((-sv / 2 + j * sv / nv) * flipv) as f32;
                vert[w] = pw as f32;

                self.positions.push(vert);

                let mut normal = Vec4::new(0.0, 0.0, 0.0, 0.0);

                normal[w] = (pw / pw.abs()) as f32;
                self.normals.push(normal);


                let tex_coord = Vec2::new((i / nu) as f32, (1 - j / nv) as f32);

                self.uvs.push(tex_coord);


                self.vertex_index += 1;
            }
        }

        for j in 0..nv {
            for i in 0..nu {
                let n = vert_shift + j * (nu + 1) + i;
                self.indices.push(n as u32);
                self.indices.push((n + nu + 1) as u32);
                self.indices.push((n + nu + 2) as u32);


                self.indices.push(n as u32);
                self.indices.push((n + nu + 2) as u32);
                self.indices.push((n + 1) as u32);
            }
        }
    }
}


/// Ported from work of Robert Hodgin(@flight404)
/// https://github.com/flight404/Eyeo2012
pub struct BoxRoom {

}

impl BoxRoom {
    pub fn new()->Self {
        let x = 1.0 as f32;
        let y = 1.0 as f32;
        let z = 1.0 as f32;

        let verts = vec![
            vec4(-x,-y,-z,1.0 ), vec4(-x,-y, z,1.0 ),
            vec4( x,-y, z,1.0 ), vec4( x,-y,-z,1.0 ),
            vec4(-x, y,-z,1.0 ), vec4(-x, y, z,1.0 ),
            vec4( x, y, z,1.0 ), vec4( x, y,-z,1.0 )
        ];

        let v_indices = vec![
            [0,1,3], [1,2,3],	// floor
            [4,7,5], [7,6,5],	// ceiling
            [0,4,1], [4,5,1],	// left
            [2,6,3], [6,7,3],	// right
            [1,5,2], [5,6,2],	// back
            [3,7,0], [7,4,0]  // front
        ];

        let v_normals = vec![
            vec4( 0.0, 1.0, 0.0, 1.0 ),	// floor
            vec4( 0.0,-1.0, 0.0, 1.0 ),	// ceiling
            vec4( 1.0, 0.0, 0.0, 1.0 ),	// left
            vec4(-1.0, 0.0, 0.0, 1.0 ),	// right
            vec4( 0.0, 0.0,-1.0, 1.0 ),	// back
            vec4( 0.0, 0.0, 1.0, 1.0 ) 	// front
        ];

        let v_tex_coords = vec![
            vec2( 0.0, 0.0),
            vec2( 0.0, 1.0),
            vec2( 1.0, 1.0),
            vec2( 1.0, 0.0)
        ];

        let t_indices = vec![
            [0,1,3], [1,2,3],	// floor
            [0,1,3], [1,2,3],	// ceiling
            [0,1,3], [1,2,3],	// left
            [0,1,3], [1,2,3],	// right
            [0,1,3], [1,2,3],	// back
            [0,1,3], [1,2,3] 	// front
        ];

        let mut positions = vec![];
        let mut indices = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        #[allow(unused)]
        let mut index = 0;
        for i in 0..12 {
            positions.push(verts[v_indices[i][0]]);
            positions.push(verts[v_indices[i][1]]);
            positions.push(verts[v_indices[i][2]]);

            indices.push(index+=1 as u32);
            indices.push(index+=1 as u32);
            indices.push(index+=1 as u32);

            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);

            uvs.push(v_tex_coords[t_indices[i][0]]);
            uvs.push(v_tex_coords[t_indices[i][1]]);
            uvs.push(v_tex_coords[t_indices[i][2]]);
        }


        BoxRoom{}
    }
}