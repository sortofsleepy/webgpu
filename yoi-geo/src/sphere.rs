use std::f32::consts::PI;
use glam::{Mat4, vec3, vec2, vec4};
use crate::traits::Geometry;

/// A basic sphere. Ported from
/// https://github.com/glo-js/primitive-sphere
pub struct PrimitiveSphere {}

impl PrimitiveSphere {
    pub fn create(radius: f32, segments: u32) -> Geometry {

        #[allow(unused)]
        let mut mat_rot_y = Mat4::IDENTITY;

        #[allow(unused)]
        let mut mat_rot_z = Mat4::IDENTITY;
        let up = vec3(0.0, 1.0, 0.0);

        #[allow(unused)]
        let mut tmp_vec = vec3(0.0, 0.0, 0.0);

        let total_z_rotation_steps = 3 + segments;
        let total_y_rotation_steps = 3 * total_z_rotation_steps;

        let mut positions = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];
        let mut indices = vec![];

        for z_rotation_step in 0..total_z_rotation_steps {
            let normalized_z = (z_rotation_step as f32) / (total_z_rotation_steps as f32);
            let angle_z = normalized_z * PI;

            for y_rotation_step in 0..total_y_rotation_steps {
                let normalized_y = (y_rotation_step as f32) / (total_y_rotation_steps as f32);
                let angle_y = normalized_y * PI * 2.0;

                mat_rot_z = Mat4::from_rotation_z(-angle_z);
                mat_rot_y = Mat4::from_rotation_y(angle_y);

                tmp_vec = mat_rot_z.transform_vector3(up);
                tmp_vec = mat_rot_y.transform_vector3(tmp_vec);

                tmp_vec.x *= -radius;
                tmp_vec.y *= -radius;
                tmp_vec.z *= -radius;
                let pos = tmp_vec.clone();
                positions.push(vec4(pos.x,pos.y,pos.z,1.0));

                // make normals
                // remember to normalize in shader or elsewhere to be 0.0 - 1.0
                let normal = tmp_vec.clone();
                normals.push(vec4(normal.x,normal.y,normal.z,1.0));

                // make uvs
                uvs.push(vec2(normal.y, normal.z));
            }


            if z_rotation_step > 0 {
                let vertices_count = positions.len();
                let mut first_index = vertices_count - 2 * (total_y_rotation_steps as usize) + 1;

                #[allow(unused)]
                for i in (first_index + (total_y_rotation_steps as usize) + 2)..vertices_count {
                    indices.push(first_index as u32);
                    indices.push((first_index + 1) as u32);
                    indices.push((first_index + (total_y_rotation_steps as usize) + 1) as u32);

                    indices.push((first_index + (total_y_rotation_steps as usize) + 1) as u32);
                    indices.push((first_index + 1) as u32);
                    indices.push((first_index + (total_y_rotation_steps as usize) + 2) as u32);

                    first_index += 1;
                }
            }
        }

        Geometry{
            positions,
            uvs,
            normals,
            indices
        }
    }
}