use glam::{Vec4, vec4};

pub mod cube;
pub mod traits;
pub mod planegeometry;
pub mod simpleshapes;
pub mod icosahedron;
pub mod dequan;
pub mod room;
pub mod sphere;
pub mod tessellate;
pub mod loaders;

/// Converts indexed vec4 attribute to non-indexed. Returns vector of new attribute values.
/// Based on Three.js implementation.
/// https://github.com/mrdoob/three.js/blob/master/src/core/BufferGeometry.js#L754
pub fn to_non_indexed(attrib: Vec<Vec4>, indices: Vec<u32>) -> Vec<Vec4> {

    // first unroll attribute
    let mut array = vec![];
    for v in attrib.iter() {
        array.push(v.x);
        array.push(v.y);
        array.push(v.z);
        array.push(v.w);
    }

    let item_size: usize = 4;
    let mut array2 = Vec::new();
    array2.resize(indices.len() * item_size, 0.0);

    let mut index = 0;
    let mut index2 = 0;
    for i in 0..indices.len(){
        index = (indices[i] as usize) * item_size;

        array2[index2] = array[index];
        array2[index2 + 1] = array[index + 1];
        array2[index2 + 2] = array[index + 2];
        array2[index2 + 3] = array[index + 3];

        index2 += item_size;
    }

    let mut fin = vec![];
    for i in (0..array2.len()).step_by(4) {
        let v = vec4(
            array2[i],
            array2[i + 1],
            array2[i + 2],
            1.0,
        );

        fin.push(v);
    }

    fin
}