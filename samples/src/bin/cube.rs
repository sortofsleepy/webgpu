use std::any::TypeId;
#[allow(unused)]
use winit::event_loop::EventLoop;
use std::borrow::Cow;
use glam::Vec4;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::window::Window;
use yoi_core::app::{App, Yoi};
use yoi_math::core::pack_vec4;
use yoi_wgpu::bindgroup::{BindGroup, BindGroupEntry, BindGroupLayoutEntry};
use yoi_wgpu::displayobject::DisplayObject;
use yoi_wgpu::framework::camera::Camera;
use yoi_wgpu::pipeline::{generate_default_pipeline_layout};
use yoi_wgpu::prelude::Vbo;
use yoi_wgpu::shader::{create_shader_module, generate_wgsl, ShaderDesc};
use yoi_wgpu::webgpuinstance::WebGPUInstance;

struct Model {
    mesh: DisplayObject2,
}

struct DisplayObject2 {
    pipeline: wgpu::RenderPipeline,
    vbos: Vec<Vbo>,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    test_index_buffer: Vbo,
    index_length: usize,
    bind_group: BindGroup,
}


fn main() {
    App::new(String::from("MultiPass"), 1024, 768)
        .setup(setup)
        .draw(draw)
        .run();
}


fn setup(_app: &Yoi, window: &winit::window::Window, _instance: &WebGPUInstance) -> Model {
    let mesh = build_mesh(_instance);

    Model {
        mesh
    }
}

fn draw(_app: &Yoi, _model: &mut Model, _instance: &WebGPUInstance) {
    let device = _instance.get_device();
    let surface = _instance.get_surface();

    let render_pipeline = &mut _model.mesh.pipeline;
    let vbos = &mut _model.mesh.vbos;
    //let index = &mut _model.mesh.index_buffer;
    let index = &mut _model.mesh.test_index_buffer;

    let len = &mut _model.mesh.index_length;
    let positions = &mut _model.mesh.vertex_buffer;
    let bg = &mut _model.mesh.bind_group;
    let queue = _instance.get_queue();


    let frame = surface
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    let mut encoder =
        device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
    {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        rpass.set_pipeline(&render_pipeline);

        rpass.set_bind_group(0, bg.get_bindgroup(), &[]);
        for i in 0..vbos.len() {
            let vbo = vbos[i].get_raw_buffer().slice(..);
            rpass.set_vertex_buffer(i as u32, vbo);
        }

        rpass.set_index_buffer(index.get_raw_buffer().slice(..), wgpu::IndexFormat::Uint16);
        rpass.draw_indexed(0..*len as u32, 0, 0..1);
    }

    queue.submit(Some(encoder.finish()));
    frame.present();
}

fn build_mesh2(instance: &WebGPUInstance) -> DisplayObject {
    let verts = vec![
        // top
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(1.0, -1.0, 1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),

        // bottom
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, -1.0, -1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),

        // right
        Vec4::new(1.0, -1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),
        Vec4::new(1.0, -1.0, 1.0, 1.0),

        // left
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),

        // front
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),

        // back
        Vec4::new(1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),
        Vec4::new(1.0, -1.0, -1.0, 1.0),
    ];
    let mut obj = DisplayObject::new();
    obj.add_attrib4(instance,verts,0);


    obj
}


fn build_mesh(instance: &WebGPUInstance) -> DisplayObject2 {
    let verts = vec![
        // top
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(1.0, -1.0, 1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),

        // bottom
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, -1.0, -1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),

        // right
        Vec4::new(1.0, -1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),
        Vec4::new(1.0, -1.0, 1.0, 1.0),

        // left
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),

        // front
        Vec4::new(1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, 1.0, -1.0, 1.0),
        Vec4::new(-1.0, 1.0, 1.0, 1.0),
        Vec4::new(1.0, 1.0, 1.0, 1.0),

        // back
        Vec4::new(1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, -1.0, 1.0, 1.0),
        Vec4::new(-1.0, -1.0, -1.0, 1.0),
        Vec4::new(1.0, -1.0, -1.0, 1.0),
    ];
    let positions = pack_vec4(verts);

    let mut vertices = Vbo::new(instance, BufferInitDescriptor {
        label: None,
        contents: bytemuck::cast_slice(&positions),
        usage: wgpu::BufferUsages::VERTEX,
    });

    vertices.add_layout_attrib(wgpu::VertexAttribute {
        format: wgpu::VertexFormat::Float32x4,
        offset: 0,
        shader_location: 0,
    });

    let vert_buf = instance.get_device().create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(&positions),
        usage: wgpu::BufferUsages::VERTEX,
    });

    let index_data: &[u16] = &[
        0, 1, 2, 2, 3, 0, // top
        4, 5, 6, 6, 7, 4, // bottom
        8, 9, 10, 10, 11, 8, // right
        12, 13, 14, 14, 15, 12, // left
        16, 17, 18, 18, 19, 16, // front
        20, 21, 22, 22, 23, 20, // back
    ];

    let index_buf = instance.get_device().create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(&index_data),
        usage: wgpu::BufferUsages::INDEX,
    });

    let index_buf_test = Vbo::new(instance, BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(&index_data),
        usage: wgpu::BufferUsages::INDEX,
    });

    #[cfg(target_arch = "wasm32")]
        let shader = create_shader_module(instance, ShaderDesc {
        label: None,
        source: generate_wgsl(include_str!("../../shaders/cube-web.wgsl")),
    });
    #[cfg(target_arch = "x86_64")]
        let shader = create_shader_module(instance, ShaderDesc {
        label: None,
        source: generate_wgsl(include_str!("../../shaders/cube.wgsl")),
    });

    //////////////////////


    let mut camera = Camera::new();
    camera.create_perspective(60.0, 1024.0 / 768.0, 0.1, 1000.0);
    camera.set_zoom(-4.0);
    let matrices = camera.get_vp().to_cols_array();

    let uniform_buf = Vbo::new(instance, BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(&matrices),
        usage: wgpu::BufferUsages::UNIFORM,
    });

    let mut bg = BindGroup::new();

    bg.add_buffer_layout_entry(&BindGroupEntry {
        binding: 0,
        visibility: wgpu::ShaderStages::VERTEX,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Uniform,
            has_dynamic_offset: false,
            min_binding_size: None,
        },
    });

    bg.build(instance, &[
        uniform_buf.as_entire_binding()
    ]);

    let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[bg.get_layout()],
        push_constant_ranges: &[],
    });

    let vertex_buffers = [vertices.get_vbo_layout()];

    let swapchain_format = instance.get_default_texture_format();
    let pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &vertex_buffers,
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(swapchain_format.into())],
        }),
        primitive: wgpu::PrimitiveState {
            cull_mode: Some(wgpu::Face::Back),
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    });

    DisplayObject2 {
        pipeline,
        vertex_buffer: vert_buf,
        vbos: vec![vertices],
        test_index_buffer: index_buf_test,
        index_buffer: index_buf,
        index_length: index_data.len(),
        bind_group: bg,
    }
}