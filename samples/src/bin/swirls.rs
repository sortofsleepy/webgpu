use winit::event_loop::EventLoop;
use std::borrow::Cow;
use std::f32::consts::PI;
use glam::{DVec4, dvec4, Vec2, vec2, Vec4, vec4};
use wgpu::{BindGroupLayoutEntry, BindingResource};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::window::Window;
use yoi_core::app::{App, Yoi};
use yoi_core::file::load_file;
use yoi_math::core::{pack_vec4, rand_float};
use yoi_wgpu::bindgroup::{BindGroup, BindGroupEntry, GroupLayout};
use yoi_wgpu::displayobject::DisplayObject;
use yoi_wgpu::framework::camera::Camera;
use yoi_wgpu::prelude::Vbo;
use yoi_wgpu::shader::{create_shader_module, generate_wgsl, ShaderDesc};
use yoi_wgpu::webgpuinstance::WebGPUInstance;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::matrix::{Matrix, Matrix4};
use yoi_wgpu::framework::framebuffer::Fbo;
use yoi_wgpu::pipeline::RasterPipeline;
use yoi_wgpu::texture::Texture;
use bytemuck::{Pod, Zeroable};

const NUM_MESHES: i32 = 3;
const WINDOW_WIDTH: u32 = 1024;
const WINDOW_HEIGHT: u32 = 768;

struct Model {
    meshes: Vec<SwirlMesh>,
    render_quad: DisplayObject,
    render_bindgroup: BindGroup,
    scene_buffer: Fbo,

}

struct SwirlMesh {
    mesh: DisplayObject,
    rotation: f32,
    model_matrix: Matrix4,
    transform_buffer: wgpu::Buffer,
    bind_group: BindGroup,
    offset: f32,
}


#[repr(C)]
#[derive(Copy, Clone, Debug, PartialEq, Pod, Zeroable)]
struct BlurSettings {
    // x, y = offset
    // z = attenuation
    blur_values: [f32; 4],
}

struct PostProcess {
    horizontal: DisplayObject,
    vertical: DisplayObject,
}


fn main() {
    App::new(String::from("Swirl"), WINDOW_WIDTH, WINDOW_HEIGHT)
        .setup(setup)
        .draw(draw)
        .run();
}


fn setup(_app: &Yoi, window: &winit::window::Window, _instance: &WebGPUInstance) -> Model {
    let camera = build_camera(_instance);

    // build fbo
    let mut scene_buffer = Fbo::new();
    scene_buffer.add_color_attachment(WINDOW_WIDTH, WINDOW_HEIGHT);
    scene_buffer.build(_instance, &[]);


    let meshes = build_mesh(_instance, &camera, &scene_buffer);


    //// BUILD RENDER PLANE /////////

    let mut bg = BindGroup::new();
    bg.add_buffer_layout_entry(BindGroupEntry {
        binding: 0,
        visibility: wgpu::ShaderStages::FRAGMENT,
        buffer_desc: GroupLayout {
            binding_type: wgpu::BindingType::Texture {
                sample_type: wgpu::TextureSampleType::Float { filterable: true },
                view_dimension: wgpu::TextureViewDimension::D2,
                multisampled: false,
            },
            count: None,
        },
    });
    bg.add_buffer_layout_entry(BindGroupEntry {
        binding: 1,
        visibility: wgpu::ShaderStages::FRAGMENT,
        buffer_desc: GroupLayout {
            binding_type: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
            count: None,
        },
    });

    let sampler = _instance.get_default_sampler();
    bg.build(_instance, &[
        wgpu::BindingResource::TextureView(scene_buffer.get_color_view(0)),
        wgpu::BindingResource::Sampler(&sampler)
    ]);

    // build render quad
    let mut render_quad = DisplayObject::new();
    render_quad.set_shader(_instance, load_file("shaders/render_quad.wgsl"));
    render_quad.compile_with_bindgroup(_instance, &bg);
    render_quad.set_num_vertices(3);

    Model {
        meshes,
        render_quad,
        scene_buffer,
        render_bindgroup: bg,
    }
}

/// Build camera buffer
fn build_camera(instance: &WebGPUInstance) -> Vbo {
    let mut camera = Camera::new();
    camera.create_perspective(PI / 4.0, WINDOW_WIDTH as f32 / WINDOW_HEIGHT as f32, 0.1, 1000.0);
    camera.set_zoom(-4.0);
    let matrices = camera.get_vp().to_cols_array();

    let camera_buffer = Vbo::new(instance, BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(&matrices),
        usage: wgpu::BufferUsages::UNIFORM,
    });

    camera_buffer
}

fn draw(_app: &Yoi, _model: &mut Model, instance: &WebGPUInstance) {
    let device = instance.get_device();
    let surface = instance.get_surface();
    let meshes = &mut _model.meshes;
    let render_quad = &mut _model.render_quad;
    let queue = instance.get_queue();
    let fbo = &mut _model.scene_buffer;
    let render_bindgroup = &mut _model.render_bindgroup;

    // Start rendering process
    let frame = surface
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    let mut encoder =
        device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });


    render_quad.draw(&mut encoder, &view, &[&render_bindgroup]);

    for obj in meshes {
        obj.rotation += (obj.offset * 0.0001).sin();
        obj.model_matrix.rotate(obj.rotation.to_radians(), [0.0, 0.0, 1.0]);

        queue.write_buffer(&obj.transform_buffer, 0, bytemuck::cast_slice(&obj.model_matrix.get_raw().to_cols_array()));

        obj.mesh.draw_debug(&mut encoder, &fbo.get_color_view(0), &[&obj.bind_group]);
        //obj.mesh.draw(&mut encoder, &view, &[&obj.bind_group]);
    }

    queue.submit(Some(encoder.finish()));
    frame.present();
}

fn build_post(instance: &WebGPUInstance) -> PostProcess {
    let w = WINDOW_WIDTH as f32;
    let h = WINDOW_HEIGHT as f32;


    /////////// BUILD HORIZONTAL PASS ///////////
    let h_settings = BlurSettings {
        blur_values: [
            1.0 / w,
            0.0,
            2.0,
            0.0
        ]
    };

    let h_settings_buffer = instance.get_device().create_buffer_init(&BufferInitDescriptor {
        label: Some("Horizontal Blur buffer"),
        contents: bytemuck::cast_slice(&[h_settings]),
        usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
    });

    let mut h_bg = BindGroup::new();
    h_bg.add_buffer_layout_entry(BindGroupEntry {
        binding: 0,
        visibility: wgpu::ShaderStages::FRAGMENT,
        buffer_desc: GroupLayout {
            binding_type: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        },
    });
    h_bg.build(instance, &[
        h_settings_buffer.as_entire_binding()
    ]);

    let mut blur_h = DisplayObject::new();
    blur_h.set_shader(instance, load_file("shaders/blur.wgsl"));
    blur_h.compile_with_bindgroup(instance, &h_bg);
    blur_h.set_num_vertices(3);

    /////////// BUILD VERTICAL PASS ///////////
    let v_settings = BlurSettings {
        blur_values: [
            0.0,
            1.0 / h,
            2.0,
            0.0
        ]
    };

    let v_settings_buffer = instance.get_device().create_buffer_init(&BufferInitDescriptor {
        label: Some("Horizontal Blur buffer"),
        contents: bytemuck::cast_slice(&[v_settings]),
        usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
    });

    let mut v_bg = BindGroup::new();
    v_bg.add_buffer_layout_entry(BindGroupEntry {
        binding: 0,
        visibility: wgpu::ShaderStages::FRAGMENT,
        buffer_desc: GroupLayout {
            binding_type: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        },
    });
    v_bg.build(instance, &[
        v_settings_buffer.as_entire_binding()
    ]);

    let mut blur_v = DisplayObject::new();
    blur_v.set_shader(instance, load_file("shaders/blur.wgsl"));
    blur_v.compile_with_bindgroup(instance, &h_bg);
    blur_v.set_num_vertices(3);

    PostProcess {
        horizontal: blur_h,
        vertical: blur_v,
    }
}

fn build_mesh(instance: &WebGPUInstance, camera: &Vbo, fbo: &Fbo) -> Vec<SwirlMesh> {
    let mut meshes = vec![];


    // setup colors
    let mut color = vec![
        Vec4::new(1.0, 0.0, 0.0, 1.0),
        Vec4::new(1.0, 1.0, 0.0, 1.0),
        Vec4::new(1.0, 0.0, 1.0, 1.0),
    ];


    for i in 0..NUM_MESHES {
        let mut mesh = DisplayObject::new();

        let triangle = SimpleShapes::generate_triangle();

        // re-work positions so we can store some extra information like an offset.
        let mut pos = vec![];

        let mut colors = vec![];

        let offset = rand_float(0.0, 45.0);

        for i in 0..triangle.positions.len() {
            let mut p = triangle.positions[i];
            p.z = offset;
            p.w = ((i + 1) as f32) * 2.0; // scale
            pos.push(p);

            colors.push(color[i]);
        }


        mesh.add_attrib4(instance, pos, 0);
        mesh.add_attrib4(instance, colors, 1);
        mesh.add_index_data(instance, &triangle.indices[..]);
        mesh.set_shader(instance, load_file("shaders/swirl.wgsl"));

        // build bind group for each mesh
        let mut bg = BindGroup::new();

        // build model matrix buffer
        let m = Matrix4::new();

        // build transform buffer
        let transform_buffer = instance.get_device().create_buffer_init(&BufferInitDescriptor {
            label: Some("Model Matrix buffer"),
            contents: bytemuck::cast_slice(&m.get_raw().to_cols_array()),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        // add transform buffer for each bind group
        bg.add_buffer_layout_entry(
            BindGroupEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::VERTEX,
                buffer_desc: GroupLayout {
                    binding_type: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
            }
        );

        bg.add_buffer_layout_entry(
            BindGroupEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::VERTEX,
                buffer_desc: GroupLayout {
                    binding_type: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
            }
        );

        bg.build(instance, &[
            camera.get_raw_buffer().as_entire_binding(),
            transform_buffer.as_entire_binding()
        ]);

        //mesh.add_target_format(instance.get_default_texture_format().into());
        mesh.add_target_format(fbo.get_color_state());
        mesh.compile_with_bindgroup(instance, &bg);

        meshes.push(SwirlMesh {
            mesh,
            rotation: 0.0,
            model_matrix: Matrix4::new(),
            transform_buffer,
            bind_group: bg,
            offset,
        });
    }

    meshes
}

/*
Example of how to build pipeline to share :

 let swapchain_format = _instance.get_default_texture_format();


    // build render quad
    let mut render_quad = DisplayObject::new();
    let triangle = SimpleShapes::generate_fullscreen_triangle();
    render_quad.add_attrib4(_instance, triangle.positions, 0);

    let shader = create_shader_module(_instance, ShaderDesc {
        label: None,
        source: generate_wgsl(load_file("shaders/render_quad.wgsl").as_str()),
    });

    let mut renderpass = RasterPipeline::new();
    let attribs = render_quad.get_attribute_layout();
    renderpass.build_pipeline(
        _instance,
        wgpu::VertexState{
            module: &shader,
            entry_point: "vs_main",
            buffers: &attribs,
        },
        Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(swapchain_format.into())],
        }),
        None

    );

    let pipeline = Rc::new(renderpass);
    render_quad.set_pipeline(pipeline);

 */