use std::borrow::Cow;
use wgpu::RenderPipeline;
use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Window,
};
//use yoi_core::app::{App,Yoi};
use yoi_core::app::{App,Yoi};
use yoi_core::file::load_file;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_wgpu::displayobject::DisplayObject;
use yoi_wgpu::prelude::WebGPUInstance;
use yoi_wgpu::shader::{create_shader_module, generate_wgsl, ShaderDesc};

struct Model {
    pipeline:RenderPipeline
}

fn setup(_app: &Yoi, window: &Window, instance: &WebGPUInstance) -> Model {

    // Load the shaders from disk
    let shader = instance.get_device().create_shader_module(wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("../../shaders/triangle.wgsl"))),
    });


    let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[],
        push_constant_ranges: &[],
    });

    let render_pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(instance.get_default_texture_format().into())],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    });

    Model{
        pipeline:render_pipeline
    }
}

fn draw(_app: &Yoi, model: &mut Model, instance: &WebGPUInstance) {
    let render_pipeline = &mut model.pipeline;
    let frame = instance.get_surface()
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");
    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());
    let mut encoder =
        instance.get_device().create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });


    {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::GREEN),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });
        rpass.set_pipeline(&render_pipeline);
        rpass.draw(0..3, 0..1);
    }

    let buffer = encoder.finish();

    instance.get_queue().submit(Some(buffer));
    frame.present();

}

fn main() {

    App::new(String::from("MultiPass"), 1024, 768)
        .setup(setup)
        .draw(draw)
        .run();

}


