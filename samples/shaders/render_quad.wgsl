
// Note that this follows a Vulkan based method used by Sacha Wihelms - may not be usable on other backends.
struct vertexOutput {
    @builtin(position) Position : vec4<f32>,
    @location(0) vUv:vec2<f32>
};

@vertex
fn vs_main(
    @builtin(vertex_index) gl_VertexIndex: u32
) -> vertexOutput {

    let _e5 = gl_VertexIndex;
    let _e12 = gl_VertexIndex;
    let uv = vec2<f32>(f32(((_e5 << u32(1)) & u32(2))), f32((_e12 & u32(2))));

    var v:vertexOutput;
    v.Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);
    v.vUv = uv;
    return v;
}

@binding(0) @group(0) var texture: texture_2d<f32>;
@binding(1) @group(0) var _sampler: sampler;

@fragment
fn fs_main(v:vertexOutput) -> @location(0) vec4<f32> {

    let uv = vec2<f32>(v.vUv.x,v.vUv.y);

    let tex = textureSample(texture,_sampler,uv);
    //return vec4<f32>(uv,0.0,1.0);
    return tex;
}