struct vertexOutput {
    @builtin(position) Position : vec4<f32>,
    @location(0) Color:vec4<f32>
};

/// Camera matrix for the mesh
struct CameraUniforms {
   viewProjectionMatrix : mat4x4<f32>
};
@binding(0) @group(0) var<uniform> uniforms : CameraUniforms;

/// Model matrix of the mesh
struct MeshModelMatrix {
    Matrix : mat4x4<f32>
};
@binding(1) @group(0) var<uniform> mesh_mat : MeshModelMatrix;


@vertex
fn vs_main(
    @location(0) position:vec4<f32>,
    @location(1) color:vec4<f32>
) -> vertexOutput {
        var pos = vec4<f32>(position.xyz,1.0);
        var v:vertexOutput;

        pos.x *= pos.a;
        pos.y *= pos.a;
        v.Color = color;
        v.Position = uniforms.viewProjectionMatrix * mesh_mat.Matrix * vec4<f32>(pos.xy,0.0,1.0);
        return v;
}


@fragment
fn fs_main(v:vertexOutput) -> @location(0) vec4<f32> {
    return vec4<f32>(v.Color.xyz,1.0);
}