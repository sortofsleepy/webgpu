
// Note that this follows a Vulkan based method used by Sacha Wihelms - may not be usable on other backends.
struct vertexOutput {
    @builtin(position) Position : vec4<f32>,
    @location(0) vUv:vec2<f32>
};

@vertex
fn vs_main(
    @builtin(vertex_index) gl_VertexIndex: u32,
    @location(0) position:vec4<f32>
) -> vertexOutput {

    let _e5 = gl_VertexIndex;
    let _e12 = gl_VertexIndex;
    let uv = vec2<f32>(f32(((_e5 << u32(1)) & u32(2))), f32((_e12 & u32(2))));

    var v:vertexOutput;
    v.Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);
    v.vUv = uv;
    return v;
}
///////////////

struct BloomSettings {
    // x,y = sample_offset
    // z = attenuation
    params:vec4<f32>;
};

@binding(2) @group(0) var<uniform> uniforms : BloomSettings;
@group(0) @binding(0) var myTexture: texture_2d<f32>;
@group(0) @binding(1) var mySampler: sampler;

@stage(fragment)
    fn fs_main(@location(0) vUv:vec2<f32>) -> @location(0) vec4<f32>{
 var sum:vec4<f32> = vec4<f32>(0.0,0.0,0.0,0.0);
            var sample_offset:vec2<f32> = uniforms.params.xy;

            sum = sum + textureSample(myTexture, mySampler, vUv + -10.0 * sample_offset) * 0.009167927656011385;
            sum = sum + textureSample(myTexture, mySampler, vUv + -9.0 * sample_offset) * 0.014053461291849008;
            sum = sum + textureSample(myTexture, mySampler, vUv + -8.0 * sample_offset) * 0.020595286319257878;
            sum = sum + textureSample(myTexture, mySampler, vUv + -7.0 * sample_offset) * 0.028855245532226279;
            sum = sum + textureSample(myTexture, mySampler, vUv + -6.0 * sample_offset) * 0.038650411513543079;
            sum = sum + textureSample(myTexture, mySampler, vUv + -5.0 * sample_offset) * 0.049494378859311142;
            sum = sum + textureSample(myTexture, mySampler, vUv + -4.0 * sample_offset) * 0.060594058578763078;
            sum = sum + textureSample(myTexture, mySampler, vUv + -3.0 * sample_offset) * 0.070921288047096992;
            sum = sum + textureSample(myTexture, mySampler, vUv + -2.0 * sample_offset) *  0.079358891804948081;
            sum = sum + textureSample(myTexture, mySampler, vUv + -1.0 * sample_offset) * 0.084895951965930902;
            sum = sum + textureSample(myTexture, mySampler, vUv + 0.0 * sample_offset) *  0.086826196862124602;
            sum = sum + textureSample(myTexture, mySampler, vUv + 1.0 * sample_offset) * 0.084895951965930902;
            sum = sum + textureSample(myTexture, mySampler, vUv + 2.0 * sample_offset) * 0.079358891804948081;
            sum = sum + textureSample(myTexture, mySampler, vUv + 3.0 * sample_offset) * 0.070921288047096992;
            sum = sum + textureSample(myTexture, mySampler, vUv + 4.0 * sample_offset) * 0.060594058578763078;
            sum = sum + textureSample(myTexture, mySampler, vUv + 5.0 * sample_offset) * 0.049494378859311142;
            sum = sum + textureSample(myTexture, mySampler, vUv + 6.0 * sample_offset) *0.038650411513543079;
            sum = sum + textureSample(myTexture, mySampler, vUv + 7.0 * sample_offset) * 0.028855245532226279;
            sum = sum + textureSample(myTexture, mySampler, vUv + 8.0 * sample_offset) * 0.020595286319257878;
            sum = sum + textureSample(myTexture, mySampler, vUv + 9.0 * sample_offset) * 0.014053461291849008;
            sum = sum + textureSample(myTexture, mySampler, vUv + 10.0 * sample_offset) *  0.009167927656011385;
            var outColor:vec4<f32> = uniforms.params.z * sum;


    //return data;
    return outColor;

}