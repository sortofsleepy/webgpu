struct vertexOutput {
    @builtin(position) Position : vec4<f32>
};

struct CameraUniforms {
    modelViewProjectionMatrix : mat4x4<f32>
};
@binding(0) @group(0) var<uniform> uniforms : CameraUniforms;


@vertex
fn vs_main(@location(0) position:vec4<f32>) -> vertexOutput {

        var v:vertexOutput;
        var pos:vec4<f32> = position * vec4<f32>(2.0);
        v.Position = uniforms.modelViewProjectionMatrix * vec4<f32>(pos.xyz,1.0);
        return v;
}


@fragment
fn fs_main() -> @location(0) vec4<f32> {
    return vec4<f32>(1.0,0.0,1.0,1.0);
}