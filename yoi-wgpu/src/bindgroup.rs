use std::num::{NonZeroU32, NonZeroU64};
use wgpu::{BindGroupDescriptor, BindGroupLayoutDescriptor};
use crate::vbo::Vbo;
use crate::webgpuinstance::WebGPUInstance;

pub struct GroupLayout {
    pub binding_type:wgpu::BindingType,
    pub count:Option<NonZeroU32>
}

pub struct BindGroupEntry {
    pub binding: u32,
    pub visibility: wgpu::ShaderStages,
    pub buffer_desc: GroupLayout,
}

//////////////////

/// A wrapper around the concept of a bind group.
pub struct BindGroup {
    layout_buffer_entries: Vec<BindGroupEntry>,
    layout: Option<wgpu::BindGroupLayout>,
    bind_group: Option<wgpu::BindGroup>,
}

impl BindGroup {
    pub fn new() -> Self {
        BindGroup {
            layout: None,
            bind_group: None,
            layout_buffer_entries: vec![],
        }
    }

    /// Adds a layout entry to the bind group
    pub fn add_buffer_layout_entry(&mut self, entry: BindGroupEntry) {
        self.layout_buffer_entries.push(entry);
    }

    /// Returns the layout object
    pub fn get_layout(&self) -> &wgpu::BindGroupLayout {
        self.layout.as_ref().unwrap()
    }

    /// Returns the bind group object.
    pub fn get_bindgroup(&self) -> &wgpu::BindGroup {
        self.bind_group.as_ref().unwrap()
    }

    /// Compiles the bind group.
    pub fn build(&mut self, instance: &WebGPUInstance, entries: &[wgpu::BindingResource]) {
        let mut all_entries = vec![];

        ///////////// BUILD BUFFER LAYOUT ENTRIES //////////////
        let buffer_entries = self.layout_buffer_entries.iter();
        for entry in buffer_entries {

            all_entries.push(wgpu::BindGroupLayoutEntry {
                binding: entry.binding,
                visibility: entry.visibility,
                ty: entry.buffer_desc.binding_type,
                count: None,
            });
        }

        self.layout = Some(instance.get_device().create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: None,
            entries: &all_entries[..],
        }));

        /////////// BUILD BIND GROUP ////////////
        let mut all_bindgroup_entries: Vec<wgpu::BindGroupEntry> = vec![];

        let buffer_entries = self.layout_buffer_entries.iter();

        let mut counter = 0;
        for entry in buffer_entries {
            all_bindgroup_entries.push(wgpu::BindGroupEntry {
                binding: entry.binding,
                resource: entries[counter].clone(),
            });
            counter += 1;
        }

        self.bind_group = Some(
            instance.get_device().create_bind_group(&BindGroupDescriptor {
                label: None,
                layout: self.layout.as_ref().unwrap(),
                entries: &all_bindgroup_entries[..],
            })
        );
    }
}
