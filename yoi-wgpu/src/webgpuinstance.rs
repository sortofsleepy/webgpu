use wgpu::{Adapter, Device, Instance, Queue, SamplerDescriptor, Surface, TextureFormat};
use winit::{
    window::Window,
};

/// A construct around a WebGPU instance.
pub struct WebGPUInstance {
    instance: Instance,
    surface: Surface,
    adapter: Adapter,
    device: wgpu::Device,
    queue: Queue,
    surface_configuration: wgpu::SurfaceConfiguration,
    alpha_modes: Vec<wgpu::CompositeAlphaMode>,
    format: TextureFormat,
}

impl WebGPUInstance {
    pub async fn new(window: &Window) -> Self {
        let instance = wgpu::Instance::default();
        let surface = unsafe { instance.create_surface(window) }.unwrap();
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                force_fallback_adapter: false,
                // Request an adapter which can render to our surface
                compatible_surface: Some(&surface),
            })
            .await
            .expect("Failed to find an appropriate adapter");

        #[cfg(target_arch = "wasm32")]
            let device_desc = &wgpu::DeviceDescriptor {
            label: None,
            features: wgpu::Features::empty(),
            // Make sure we use the texture resolution limits from the adapter, so we can support images the size of the swapchain.
            limits: wgpu::Limits::downlevel_webgl2_defaults().using_resolution(adapter.limits()),
        };

        // TODO not sure if webgl2 limits are applicable to WASM builds or not. Use this default device descriptor for the time being.
        #[cfg(target_arch = "x86_64")]
            let device_desc = &wgpu::DeviceDescriptor {
            label: None,
            features: wgpu::Features::empty(),
            // Make sure we use the texture resolution limits from the adapter, so we can support images the size of the swapchain.
            limits: wgpu::Limits::downlevel_defaults().using_resolution(adapter.limits()),
        };

        // Create the logical device and command queue
        let (device, queue) = adapter
            .request_device(
                device_desc,
                None,
            )
            .await
            .expect("Failed to create device");


        let swapchain_capabilities = surface.get_capabilities(&adapter);
        let swapchain_format = swapchain_capabilities.formats[0];


        let alpha_mode_main = swapchain_capabilities.alpha_modes[0];
        let size = window.inner_size();
        WebGPUInstance {
            instance,
            surface,
            adapter,
            device,
            queue,
            format: swapchain_capabilities.formats[0],
            alpha_modes: swapchain_capabilities.alpha_modes,
            surface_configuration: wgpu::SurfaceConfiguration {
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                format: swapchain_format,
                width: size.width,
                height: size.height,
                present_mode: wgpu::PresentMode::Mailbox,
                alpha_mode: alpha_mode_main,
                view_formats: vec![],
            },
        }
    }

    pub fn set_surface_width(&mut self, w:u32){
        self.surface_configuration.width = w;
    }

    pub fn set_surface_height(&mut self, h:u32){
        self.surface_configuration.height = h;
    }

    /// returns a reference to the WebGPU device.
    pub fn get_device(&self) -> &Device {
        &self.device
    }

    pub fn get_adapter(&self)->&Adapter {
        &self.adapter
    }

    /// returns the default swapchain format
    pub fn get_default_texture_format(&self) -> TextureFormat {
        self.format
    }

    pub fn configure_instance_surface(&self, width: u32, height: u32) {
        let swapchain_format = self.get_default_texture_format();
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: swapchain_format,
            width: width,
            height: height,
            present_mode: wgpu::PresentMode::Mailbox,
            alpha_mode: self.alpha_modes[0],
            view_formats: vec![],
        };

        self.surface.configure(&self.device, &config);
    }

    /// Constructs a default sampler object.
    pub fn get_default_sampler(&self) -> wgpu::Sampler {
        self.device.create_sampler(&SamplerDescriptor::default())
    }

    /// return the instance's queue.
    pub fn get_queue(&self) -> &wgpu::Queue
    {
        &self.queue
    }

    /// returns the instance's surface
    pub fn get_surface(&self) -> &wgpu::Surface {
        &self.surface
    }

    /// generates a render pipeline based on the descriptor that you pass in
    pub fn generate_render_pipeline(&self, descriptor: &wgpu::RenderPipelineDescriptor) -> wgpu::RenderPipeline {
        self.device.create_render_pipeline(descriptor)
    }

    /// generates a bind group.
    pub fn create_bind_group(&self, desc: wgpu::BindGroupDescriptor) -> wgpu::BindGroup {
        self.device.create_bind_group(&desc)
    }
}