use glam::Vec4;

/// unrolls a
pub fn pack_vec4(_vec:Vec<Vec4>) -> Vec<[f32;4]> {
    let mut bytes = vec![];
    for v in _vec {
        bytes.push(v.to_array())
    }

    bytes
}