use std::ops::Mul;
use glam::{Affine3A, Mat4, Vec3A};

pub trait Matrix {
    /// translates matrix.
    fn translate(&mut self, vec:Vec3A);

    /// rotates matrix.
    fn rotate(&mut self, vec:Vec3A);
}

/// wrapper around Glam Mat4
#[derive(Copy,Clone)]
pub struct Matrix4 {
    matrix:Mat4,
    translation:Affine3A,
    rotation:Affine3A,
    scale:Affine3A
}



impl Matrix for Matrix4 {

    /// translates the matrix.
    fn translate(&mut self,vec:Vec3A){

        // x is reversed for some reason, flip it to follow OpenGL conventions.
        let mut v = vec;
        v.x = v.x * -1.0;
        self.translation.translation = v;
        self.matrix = self.matrix.mul(self.translation);
    }

    /// rotates the matrix.
    /// TODO this probably isn't right, need to fix at some point.
    fn rotate(&mut self,rot:Vec3A){
        self.rotation.translation = rot;
        self.matrix = self.matrix.mul(self.rotation);
    }
}

impl Matrix4{
    pub fn new()->Self {
        Matrix4{
            matrix:Mat4::IDENTITY,
            translation:Affine3A::IDENTITY,
            rotation:Affine3A::IDENTITY,
            scale:Affine3A::IDENTITY
        }
    }

    pub fn get_matrix(&self)->Mat4{
        self.matrix
    }
}