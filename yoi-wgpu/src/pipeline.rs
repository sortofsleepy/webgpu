use std::num::NonZeroU32;
use wgpu::{DepthStencilState, RenderPipelineDescriptor, ShaderModule, TextureFormat};
use crate::shader::ShaderDesc;
use crate::webgpuinstance::WebGPUInstance;


/// Maintains information about generating a regular [wgpu::RenderPipeline] object.
pub struct RasterPipeline {
    bind_group_layouts: Vec<wgpu::BindGroupLayout>,
    push_constant_ranges: Vec<wgpu::PushConstantRange>,
    primitive_state: wgpu::PrimitiveState,
    pipeline: Option<wgpu::RenderPipeline>,
    depth_stencil_state: Option<DepthStencilState>,
    multiview: Option<NonZeroU32>,
    multisample_state: wgpu::MultisampleState
}

impl RasterPipeline {
    pub fn new() -> Self {
        RasterPipeline {
            bind_group_layouts: vec![],
            push_constant_ranges: vec![],
            primitive_state: wgpu::PrimitiveState {
                ..Default::default()
            },
            pipeline: None,
            depth_stencil_state: None,
            multiview: None,
            multisample_state: wgpu::MultisampleState::default(),
        }
    }


    /*
       /// Adds a bind group layout to the pipeline
    pub fn add_bindgroup_layout(mut self, bgl: &'a wgpu::BindGroupLayout) -> Self {
        self.bind_group_layouts.push(bgl);
        self
    }

    /// Adds a push constant range to the pipeline
    pub fn add_push_constant_range(mut self, pcr: &'a wgpu::PushConstantRange) -> Self {
        self.push_constant_ranges.push(pcr);
        self
    }
     */

    /// Sets the topology for a pipeline.
    pub fn set_topology(mut self, topology: wgpu::PrimitiveTopology) -> Self {
        self.primitive_state.topology = topology;
        self
    }

    /// Constructs the pipeline
    pub fn build_pipeline(
        &mut self,
        instance: &WebGPUInstance,
        vertex: wgpu::VertexState,
        fragment: Option<wgpu::FragmentState>,
        depth_stencil: Option<wgpu::DepthStencilState>,
    ) {
        let num_bgl = self.bind_group_layouts.len();
        let mut bg_layouts = vec![];

        for i in 0..num_bgl {
            bg_layouts.push(&self.bind_group_layouts[i]);
        }

        // make default pipeline
        let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &bg_layouts[..],
            push_constant_ranges: &[],
        });


        self.pipeline = Some(instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex,
            fragment,
            primitive: self.primitive_state,
            depth_stencil,
            multisample: self.multisample_state,
            multiview: self.multiview,
        }));
    }

    pub fn get_pipeline(&self) -> &wgpu::RenderPipeline {
        self.pipeline.as_ref().unwrap()
    }
}