use std::mem;
use glam::Vec4;
use wgpu::{BufferDescriptor, BufferSlice};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use crate::webgpuinstance::WebGPUInstance;

pub struct BufferLayout {
    pub stride: u64,
    pub step_mode: wgpu::VertexStepMode,
    attributes: Vec<wgpu::VertexAttribute>,
}

/// Construct around the concept of a BUffer object.
pub struct Vbo {
    raw: wgpu::Buffer,
    offset: usize,
    layout: BufferLayout,
}

impl Vbo {
    /// instantiates a new Vbo with some assumptions as to the use case for the buffer.
    pub fn new(instance: &WebGPUInstance, fmt: BufferInitDescriptor) -> Self {
        let vertex_size = (mem::size_of::<Vec4>());
        Vbo {
            raw: instance.get_device().create_buffer_init(&fmt),
            offset: 0,
            layout: BufferLayout {
                stride: vertex_size as wgpu::BufferAddress,
                step_mode: wgpu::VertexStepMode::Vertex,
                attributes: vec![],
            },
        }
    }

    /// instantiates a new Vbo and allows a custom offset to get passed.
    pub fn new_with_offset(instance: &WebGPUInstance, fmt: BufferInitDescriptor, offset: usize) -> Self {
        let vertex_size = (mem::size_of::<Vec4>());

        Vbo {
            raw: instance.get_device().create_buffer_init(&fmt),
            offset,
            layout: BufferLayout {
                stride: vertex_size as wgpu::BufferAddress,
                step_mode: wgpu::VertexStepMode::Vertex,
                attributes: vec![],
            },
        }
    }

    /// Sets the Vbo stride and step mode.
    pub fn set_vbo_stride_and_step(&mut self, stride: usize, step: wgpu::VertexStepMode) {
        self.layout.stride = stride as wgpu::BufferAddress;
        self.layout.step_mode = step;
    }

    /// Sets the stride
    pub fn set_stride(&mut self, stride: usize) {
        self.layout.stride = stride as wgpu::BufferAddress;
    }

    /// Sets the step mode.
    pub fn set_step_mode(&mut self, step_mode: wgpu::VertexStepMode) {
        self.layout.step_mode = step_mode;
    }

    /// Adds a layout attribute
    pub fn add_layout_attrib(&mut self, attrib: wgpu::VertexAttribute) {
        self.layout.attributes.push(attrib);
    }

    /// returns a VertexBufferLayout for the current settings in the VBO
    pub fn get_vbo_layout(&self) -> wgpu::VertexBufferLayout {
        wgpu::VertexBufferLayout {
            array_stride: self.layout.stride,
            step_mode: self.layout.step_mode,
            attributes: self.layout.attributes.as_slice(),
        }
    }

    pub fn as_entire_binding(&self) -> wgpu::BindingResource {
        self.raw.as_entire_binding()
    }

    /// returns the underlying buffer object.
    pub fn get_raw_buffer(&self) -> &wgpu::Buffer {
        &self.raw
    }

    /// Get BufferSlice of the Vbo
    pub fn get_buffer_sliced(&self) -> BufferSlice {
        self.raw.slice(..)
    }
}


pub fn get_size_in_bytes<T>(values: &Vec<T>) -> wgpu::BufferAddress {
    let slice_size = values.len() * std::mem::size_of::<T>();
    slice_size as wgpu::BufferAddress
}



/*
/// initializes an empty Vbo
    /// Assumes usage will be in vertex shader.
    pub fn new(instance:&WebGPUInstance)->Self {

        let contents = wgpu::util::BufferInitDescriptor{
            label: None,
            contents: &[],
            usage: wgpu::BufferUsages::VERTEX
        };

        Vbo{
            raw:instance.get_device().create_buffer_init(&contents)
        }
    }

    /// same as the normal constructor but allows you to specify usage.
    pub fn new_with_usage(instance:&WebGPUInstance,usage:wgpu::BufferUsages)->Self {

        let contents = wgpu::util::BufferInitDescriptor{
            label: None,
            contents: &[],
            usage
        };

        Vbo{
            raw:instance.get_device().create_buffer_init(&contents)
        }
    }

    /// initializes a new Vbo with some data.
    /// Assumes usage will be in vertex shader.
    /// TODO can this be made more safe?
    pub fn new_with_data<T>(instance:&WebGPUInstance, data:Vec<T>) ->Self{

        let contents = wgpu::util::BufferInitDescriptor{
            label: None,
            contents: unsafe { std::mem::transmute_copy(&data[0]) },
            usage: wgpu::BufferUsages::VERTEX
        };

        Vbo{
            raw:instance.get_device().create_buffer_init(&contents)
        }
    }
    pub fn set_usage(&self,instance:&WebGPUInstance){


    }
 */