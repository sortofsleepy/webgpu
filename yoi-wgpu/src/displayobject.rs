use std::any::TypeId;
use std::rc::Rc;
use glam::{Vec2, Vec3, Vec4};
use wgpu::{Buffer, BufferSlice, ColorTargetState, CommandEncoder, RenderPass, RenderPassColorAttachment, RenderPipeline, ShaderModule, TextureView};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use yoi_math::core::{pack_vec3, pack_vec4};
use yoi_math::utils::{is_vec3, is_vec4};
use crate::bindgroup::BindGroup;
use crate::pipeline::RasterPipeline;
use crate::prelude::{generate_wgsl, ShaderDesc, WebGPUInstance};
use crate::shader::create_shader_module;
use crate::vbo::Vbo;

pub struct DisplayObjectAttrib {
    pub vbo: Vbo,
    pub shader_location: u32,
}

/// The wrapper around the idea of a mesh.
pub struct DisplayObject {
    shader: Option<wgpu::ShaderModule>,
    attributes: Vec<DisplayObjectAttrib>,
    index_buffer: Option<Vbo>,
    pipeline_layout: Option<wgpu::PipelineLayout>,
    render_pipeline: Option<wgpu::RenderPipeline>,
    cull_mode: wgpu::Face,
    num_indices: u32,
    num_vertices: u32,
    multisample_state: wgpu::MultisampleState,
    raster_pipeline: Option<Rc::<RasterPipeline>>,
    target_formats: Vec<Option<wgpu::ColorTargetState>>,
}

impl DisplayObject {
    pub fn new() -> Self {
        DisplayObject {
            shader: None,
            attributes: vec![],
            index_buffer: None,
            pipeline_layout: None,
            cull_mode: wgpu::Face::Back,
            render_pipeline: None,
            num_indices: 0,
            num_vertices: 3,
            multisample_state: wgpu::MultisampleState::default(),
            raster_pipeline: None,
            target_formats: vec![],
        }
    }

    /// Returns whether or not indices exist.
    pub fn has_index_buffer(&self) -> bool {
        self.index_buffer.is_some()
    }

    /// Gets a buffer slice of the index buffer.
    pub fn get_index_buffer_sliced(&self) -> BufferSlice {
        self.index_buffer.as_ref().unwrap().get_raw_buffer().slice(..)
    }

    /// adds a 4 component(Vec4) attribute to the DisplayObject.
    pub fn add_attrib4(&mut self, instance: &WebGPUInstance, data: Vec<Vec4>, shader_location: u32) {
        let mut buffer = Vbo::new(instance, BufferInitDescriptor {
            label: None,
            contents: bytemuck::cast_slice(&data),
            usage: wgpu::BufferUsages::VERTEX,

        });

        buffer.add_layout_attrib(wgpu::VertexAttribute {
            format: wgpu::VertexFormat::Float32x4,
            offset: 0,
            shader_location,
        });

        self.attributes.push(DisplayObjectAttrib {
            vbo: buffer,
            shader_location,
        });
    }

    /// adds a 3 component(Vec3) attribute to the DisplayObject.
    pub fn add_attrib3(&mut self, instance: &WebGPUInstance, data: Vec<Vec3>, shader_location: u32) {
        let mut buffer = Vbo::new(instance, BufferInitDescriptor {
            label: None,
            contents: bytemuck::cast_slice(&data),
            usage: wgpu::BufferUsages::VERTEX,
        });

        buffer.add_layout_attrib(wgpu::VertexAttribute {
            format: wgpu::VertexFormat::Float32x4,
            offset: 0,
            shader_location,
        });
        self.attributes.push(DisplayObjectAttrib {
            vbo: buffer,
            shader_location,
        });
    }

    /// adds a 2 component(Vec2) attribute to the DisplayObject.
    pub fn add_attrib2(&mut self, instance: &WebGPUInstance, data: Vec<Vec2>, shader_location: u32) {
        let mut buffer = Vbo::new(instance, BufferInitDescriptor {
            label: None,
            contents: bytemuck::cast_slice(&data),
            usage: wgpu::BufferUsages::VERTEX,
        });

        buffer.add_layout_attrib(wgpu::VertexAttribute {
            format: wgpu::VertexFormat::Float32x2,
            offset: 0,
            shader_location,
        });

        self.attributes.push(DisplayObjectAttrib {
            vbo: buffer,
            shader_location,
        });
    }

    pub fn set_num_vertices(&mut self, size: u32) {
        self.num_vertices = size;
    }

    pub fn draw_debug(&mut self, encoder: &mut CommandEncoder, view: &TextureView, bind_groups: &[&BindGroup]) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        rpass.set_viewport(0.0,0.0,1024.0,768.0,0.0,1.0);
        // bind the pipeline
        rpass.set_pipeline(self.get_pipeline());

        // If there are bind groups, bind them.
        // Note that the order they are passed in signifies which slot they get attached to.
        // TODO add support for offsets
        for i in 0..bind_groups.len() {
            rpass.set_bind_group(i as u32, bind_groups[i].get_bindgroup(), &[]);
        }

        // Bind vertex buffers associated with attributes
        for attrib in &self.attributes {
            rpass.set_vertex_buffer(attrib.shader_location, attrib.vbo.get_buffer_sliced());
        }

        // Finally draw, method used depends on whether or not there is an index buffer.
        if self.index_buffer.is_some() {
            rpass.set_index_buffer(self.get_index_buffer_sliced(), wgpu::IndexFormat::Uint32);
            rpass.draw_indexed(0..self.num_indices as u32, 0, 0..1);
        } else {
            rpass.draw(0..self.num_vertices, 0..1);
        }

        drop(rpass);
    }

    /// Renders the DisplayObject.
    pub fn draw(&mut self, encoder: &mut CommandEncoder, view: &TextureView, bind_groups: &[&BindGroup]) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        // bind the pipeline
        rpass.set_pipeline(self.get_pipeline());

        // If there are bind groups, bind them.
        // Note that the order they are passed in signifies which slot they get attached to.
        // TODO add support for offsets
        for i in 0..bind_groups.len() {
            rpass.set_bind_group(i as u32, bind_groups[i].get_bindgroup(), &[]);
        }

        // Bind vertex buffers associated with attributes
        for attrib in &self.attributes {
            rpass.set_vertex_buffer(attrib.shader_location, attrib.vbo.get_buffer_sliced());
        }

        // Finally draw, method used depends on whether or not there is an index buffer.
        if self.index_buffer.is_some() {
            rpass.set_index_buffer(self.get_index_buffer_sliced(), wgpu::IndexFormat::Uint32);
            rpass.draw_indexed(0..self.num_indices as u32, 0, 0..1);
        } else {
            rpass.draw(0..self.num_vertices, 0..1);
        }

        drop(rpass);
    }

    /// Returns the raw buffer of the attribute at the specified shader location.
    pub fn get_attrib_buffer(&self, shader_location: u32) -> &Buffer {
        let mut count = 0;

        let buffer = loop {
            if self.attributes[count].shader_location == shader_location {
                break self.attributes[count].vbo.get_raw_buffer();
            }
            count += 1
        };

        buffer
    }

    /// adds an index buffer based on the index data passed in
    pub fn add_index_data(&mut self, instance: &WebGPUInstance, index_data: &[u32]) {
        self.num_indices = index_data.len() as u32;
        self.index_buffer = Some(Vbo::new(instance, BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(&index_data),
            usage: wgpu::BufferUsages::INDEX,
        }));
    }

    pub fn set_pipeline(&mut self, pipeline: Rc<RasterPipeline>) {
        self.raster_pipeline = Some(pipeline);
    }

    pub fn get_attribute_layout(&mut self) -> Vec<wgpu::VertexBufferLayout> {
        let mut attribs = vec![];
        for attrib in &self.attributes {
            attribs.push(attrib.vbo.get_vbo_layout());
        }

        attribs
    }

    /// Adds a target format for rendering.
    pub fn add_target_format(&mut self, fmt: ColorTargetState) {
        self.target_formats.push(Some(fmt));
    }

    pub fn compile_debug(&mut self, instance: &WebGPUInstance) {
        // compile attributes into usable compilation state
        let mut attribs = vec![];
        for attrib in &self.attributes {
            attribs.push(attrib.vbo.get_vbo_layout());
        }

        let mut pipeline = RasterPipeline::new();

        let swapchain_format = instance.get_default_texture_format();
        // If there are no color target states set, use default from swapchain.
        if self.target_formats.len() < 1 {
            self.target_formats.push(Some(instance.get_default_texture_format().into()))
        }

        pipeline.build_pipeline(
            instance,
            wgpu::VertexState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "vs_main",
                buffers: &attribs.as_ref(),
            },
            Some(wgpu::FragmentState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "fs_main",
                targets: &self.target_formats[..],
            }),
            None,
        );

        self.raster_pipeline = Some(Rc::new(pipeline))
    }

    /// compiles the render pipeline for the mesh.
    /// Uses the default swapchain format as the color attachment format for the render.
    pub fn compile(&mut self, instance: &WebGPUInstance) {

        // compile attributes into usable compilation state
        let mut attribs = vec![];
        for attrib in &self.attributes {
            attribs.push(attrib.vbo.get_vbo_layout());
        }

        // If there are no color target states set, use default from swapchain.
        if self.target_formats.len() < 1 {
            self.target_formats.push(Some(instance.get_default_texture_format().into()))
        }

        // make default pipeline
        let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[],
            push_constant_ranges: &[],
        });

        let pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "vs_main",
                buffers: &attribs.as_ref(),
            },
            fragment: Some(wgpu::FragmentState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "fs_main",
                targets: &self.target_formats[..],
            }),
            primitive: wgpu::PrimitiveState {
                cull_mode: Some(self.cull_mode),
                ..Default::default()
            },
            depth_stencil: None,
            multisample: self.multisample_state,
            multiview: None,
        });


        self.render_pipeline = Some(pipeline);
    }

    /// compiles the render pipeline for the mesh with the specified [BindGroup] object.
    pub fn compile_with_bindgroup(&mut self, instance: &WebGPUInstance, bind_group: &BindGroup) {

        // compile attributes into usable compilation state
        let mut attribs = vec![];
        for attrib in &self.attributes {
            attribs.push(attrib.vbo.get_vbo_layout());
        }

        // If there are no color target states set, use default from swapchain.
        if self.target_formats.len() < 1 {
            self.target_formats.push(Some(instance.get_default_texture_format().into()))
        }

        // make default pipeline
        let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[bind_group.get_layout()],
            push_constant_ranges: &[],
        });


        let pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "vs_main",
                buffers: &attribs.as_ref(),
            },
            fragment: Some(wgpu::FragmentState {
                module: self.shader.as_ref().unwrap(),
                entry_point: "fs_main",
                targets: &self.target_formats[..],
            }),
            primitive: wgpu::PrimitiveState {
                cull_mode: Some(self.cull_mode),
                ..Default::default()
            },
            depth_stencil: None,
            multisample: self.multisample_state,
            multiview: None,
        });

        self.render_pipeline = Some(pipeline);
    }

    pub fn get_attributes(&self) -> &Vec<DisplayObjectAttrib> {
        &self.attributes
    }

    /// Returns the DisplayObject's render pipeline.
    pub fn get_pipeline(&self) -> &RenderPipeline {
        self.render_pipeline.as_ref().unwrap()
    }

    /// Sets the shader for the display object.
    pub fn set_shader(&mut self, instance: &WebGPUInstance, source: String) {
        self.shader = Some(create_shader_module(instance, ShaderDesc {
            label: None,
            source: generate_wgsl(source.as_str()),
        }));
    }
}