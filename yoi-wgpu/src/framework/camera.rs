use std::rc::Rc;
use glam::{Mat4, Vec3};
use crate::prelude::{Vbo,WebGPUInstance};

pub struct Camera {
    projection:Mat4,
    view:Mat4,
    eye:Vec3,
    target:Vec3,
    up:Vec3
}

#[derive(Copy,Clone)]
pub struct CameraMatrices{
    pub proj:Mat4,
    pub view:Mat4,
}

impl Camera {

    pub fn new()->Self {
        let mut cam = Camera{
            projection:Mat4::IDENTITY,
            view:Mat4::IDENTITY,
            eye:Vec3::new(0.0,0.0,0.0),
            target:Vec3::new(0.0,0.0,0.0),
            up:Vec3::new(0.0,1.0,0.0), // y is flipped to maintain correct orientation
        };

        // set a default zoom so we can make sure to see something on the screen.
        cam.set_zoom(-0.4);

        cam
    }

    /// flips the y value in the up vector for the camera.
    /// This is necessary in some cases in Vulkan (but could also be done in-shader)
    pub fn flip_y(&mut self){
        self.up.y = self.up.y * -1.0;
    }

    /// Creates a right-handed perspective projection matrix
    pub fn create_perspective(&mut self,fov:f32, aspect:f32, near:f32, far:f32){
        self.projection = Mat4::perspective_rh(fov,aspect,near,far);
    }

    /// Returns currently calculated projection and view matrices of the camera
    pub fn getMatrices(&self)->CameraMatrices{
        CameraMatrices{
            proj:self.projection,
            view:self.view
        }
    }

    /// returns combined projection * view matrices;
    pub fn get_vp(&self)->glam::Mat4 {
        self.projection * self.view
    }

    /// Resets the projection and view matrices.
    pub fn reset(&mut self){
        self.projection = Mat4::IDENTITY;
        self.view = Mat4::IDENTITY;
    }

    /// returns the projection matrix
    pub fn get_projection(&self)->Mat4{
        self.projection.clone()
    }

    /// returns the view matrix
    pub fn get_view(&self)->Mat4{
        self.view.clone()
    }

    /// sets the zoom level of the view matrix.
    pub fn set_zoom(&mut self, zoom:f32){
        self.eye.z = zoom * -1.0; // positive is forward, negative value is backwards.
        self.view = Mat4::look_at_rh(self.eye,self.target,self.up);
    }
}