use std::f32::consts::PI;
use glam::{Affine3A, Mat4, Vec3};
use yoi_math::core::create_vec3;
use yoi_math::matrix::{Matrix, Matrix4};

pub struct OrbitCam {
    projection:Matrix4,
    view:Matrix4,
    distance:Vec3,

    // tmp matrix used to hold some calculations
    camera_matrix:Matrix4,

    position:Vec3,
    orbit_x:f32,
    orbit_y:f32,
    max_orbit_x:f32,
    max_orbit_y:f32,
    min_orbit_x:f32,
    min_orbit_y:f32,
    constrain_x_orbit:f32,
    constrain_y_orbit:f32,

    max_distance:f32,
    min_distance:f32,
    distance_step:f32,
    constrain_distance:bool,

    dirty:bool,
    moving:bool,
    last_x:f32,
    last_y:f32,
    target:Vec3

}

impl OrbitCam{
    pub fn new()->Self {
        OrbitCam{
            projection: Matrix4::IDENTITY(),
            view: Matrix4::IDENTITY(),
            distance:Vec3::new(0.0,0.0,10.0),
            camera_matrix: Matrix4::IDENTITY(),
            position: create_vec3(),
            orbit_x: 0.0,
            orbit_y: 0.0,
            max_orbit_x: 0.0,
            max_orbit_y: 0.0,
            min_orbit_x: 0.0,
            min_orbit_y: 0.0,
            constrain_x_orbit: 0.0,
            constrain_y_orbit: 0.0,
            max_distance: 0.0,
            min_distance: 0.0,
            distance_step: 0.0,
            constrain_distance: false,
            dirty: false,
            moving: false,
            last_x: 0.0,
            last_y: 0.0,
            target: create_vec3()
        }
    }

    /// sets zoom of the camera.
    pub fn set_distance(&mut self, dist:f32){
        if self.constrain_distance {
            let m = self.distance.z.max(self.min_distance);
            self.distance.z = m.min(self.max_distance);
        }else{
            self.distance.z = dist;
        }
    }

    /// calculates the new orbit for the camera
    pub fn orbit(&mut self, x:f32,y:f32){
        if !x.is_nan() || !y.is_nan() {
            self.orbit_y += x;

            if !self.constrain_y_orbit.is_nan() {
                let m = self.orbit_y.max(self.min_orbit_y);
                self.orbit_y = m.min(self.max_orbit_y);
            }
        }else {
            while self.orbit_y < (PI * -1.0){
                self.orbit_y += PI * 2.0;
            }

            while self.orbit_y >= PI{
                self.orbit_y -= PI * 2.0;
            }
        }

        self.orbit_x += y;

        if !self.constrain_x_orbit.is_nan() {
            let m = self.orbit_x.max(self.min_orbit_x);
            self.orbit_y = m.min(self.max_orbit_x);
        }else {
            while self.orbit_x < (PI * -1.0){
                self.orbit_x += PI * 2.0;
            }

            while self.orbit_x >= PI{
                self.orbit_x -= PI * 2.0;
            }
        }

        self.dirty = true;
    }

    /// updates matrices based on the current setting.
    pub fn update_matrices(&mut self){

        let mut mv = self.camera_matrix;

        let a = Affine3A::from_translation(self.target);
        let b = Affine3A::from_translation(self.distance);

        mv.translate(a.translation);
        mv.rotate( -self.orbit_y,[0.0,1.0,0.0]);
        mv.rotate( -self.orbit_x,[1.0,0.0,0.0]);
        mv.translate(b.translation);

        self.view = self.camera_matrix.invert();

        self.dirty = false;
    }

    /// gets the view matrix
    pub fn get_view_matrix(&self)->Matrix4{
        self.view
    }

    /// what to trigger when the mouse is down.
    pub fn on_mouse_down(&mut self,x:f32,y:f32) {
        self.moving = true;

        self.last_x = x;
        self.last_y = y;
    }

    pub fn on_mouse_up(&mut self){
        self.moving = false;
    }

    /// what to trigger when the mouse is moving.
    pub fn on_mouse_move(&mut self,x:f32,y:f32){
        if self.moving {
            let x_delta = x - self.last_x;
            let y_delta = y - self.last_y;

            println!("Delta is {}|{}",x_delta,y_delta );
            self.last_x = x;
            self.last_y = y;

            self.orbit(x_delta * 0.024, y_delta * 0.025)
        }
    }
}