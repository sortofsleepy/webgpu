use wgpu::ColorTargetState;
use crate::prelude::WebGPUInstance;
use crate::texture::Texture;


/// Manages a group of color attachments and a depth attachment that can be rendered into.
pub struct Fbo {
    attachments: Vec<Texture>,
    depth_format: wgpu::TextureFormat,
    color_format: wgpu::TextureFormat,
    depth_texture: Option<Texture>,
}

impl Fbo {
    pub fn new() -> Self {
        Fbo {
            attachments: vec![],
            depth_format: wgpu::TextureFormat::Depth16Unorm,
            color_format: wgpu::TextureFormat::Bgra8Unorm,
            depth_texture: None,
        }
    }

    /// Sets the depth texture format
    pub fn set_depth_format(mut self, format: wgpu::TextureFormat) -> Self {
        self.depth_format = format;
        self
    }

    /// Sets the color texture format
    pub fn set_color_format(mut self, format: wgpu::TextureFormat) -> Self {
        self.color_format = format;
        self
    }

    /// adds a color attachment.
    pub fn add_color_attachment(&mut self, width: u32, height: u32) {
        let mut color_texture = Texture::new(width, height);
        color_texture.set_format(self.color_format);

        self.attachments.push(color_texture);
    }

    pub fn build(&mut self, instance: &WebGPUInstance, view_formats: &[wgpu::TextureFormat]) {
        for i in 0..self.attachments.len() {
            self.attachments[i].build(instance, view_formats);
        }
    }

    /// adds depth texture
    pub fn add_depth_texture(mut self, width: u32, height: u32) -> Self {
        let mut depth_texture = Texture::new(width, height);
        depth_texture.set_format(self.depth_format);

        self.depth_texture = Some(depth_texture);

        self
    }

    /// Returns the color view at the specified attachment
    pub fn get_color_view(&self, idx: usize) -> &wgpu::TextureView {
        self.attachments[idx].get_raw_view()
    }

    pub fn get_color_state(&self) -> ColorTargetState {
        self.color_format.into()
    }
}