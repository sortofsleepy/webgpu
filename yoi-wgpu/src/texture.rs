use wgpu::{Extent3d, TextureDimension, TextureView};
use winit::dpi::PhysicalSize;
use crate::prelude::WebGPUInstance;

/// A wrapper around a TextureView
pub struct Texture {
    raw: Option<TextureView>,
    format: wgpu::TextureFormat,
    width: u32,
    height: u32,
    usage: wgpu::TextureUsages,
    mip_level: u32,
    sample_count: u32,
    texture_dimension: TextureDimension,
    extent: Extent3d,
}

impl Texture {
    /// Instantiates a new texture
    pub fn new(width: u32, height: u32) -> Self {
        Texture {
            width,
            height,
            raw: None,
            format: wgpu::TextureFormat::Bgra8Unorm,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            mip_level: 1,
            sample_count: 1,
            texture_dimension: TextureDimension::D2,
            extent: Extent3d {
                width,
                height,
                depth_or_array_layers: 1,
            },
        }
    }

    /// sets the format for the texture.
    pub fn set_format(&mut self, fmt: wgpu::TextureFormat) {
        self.format = fmt;
    }

    /// sets the usage for the texture.
    pub fn set_usage(&mut self, usage: wgpu::TextureUsages) {
        self.usage = usage
    }

    /// Returns the raw, underlying texture view
    pub fn get_raw_view(&self) -> &wgpu::TextureView {
        self.raw.as_ref().unwrap()
    }

    pub fn get_dimensions(&self) -> PhysicalSize<u32> {
        PhysicalSize::new(self.width, self.height)
    }

    pub fn build(
        &mut self,
        instance: &WebGPUInstance,
        view_formats: &[wgpu::TextureFormat],
    ) {
        let device = instance.get_device();

        let tex = device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size: self.extent,
            mip_level_count: self.mip_level,
            sample_count: self.sample_count,
            dimension: self.texture_dimension,
            format: self.format,
            usage: self.usage,
            view_formats,
        });

        // TODO make TextureViewDescriptor set-able
        let view = tex.create_view(&wgpu::TextureViewDescriptor::default());
        self.raw = Some(view)
    }
}