use std::borrow::Cow;
use wgpu::{Label, ShaderModule, ShaderSource};
use crate::webgpuinstance::WebGPUInstance;


/// wrapper around ShaderModuleDescriptor
pub struct ShaderDesc<'a> {
    pub label: Label<'a>,
    pub source: ShaderSource<'a>,
}

pub fn create_shader_module(instance:&WebGPUInstance, desc:ShaderDesc) -> ShaderModule {
    instance.get_device().create_shader_module(wgpu::ShaderModuleDescriptor {
        label: None,
        source: desc.source
    })
}

/// Processes and validates WGSL shader source.
pub fn generate_wgsl(src:&str)->wgpu::ShaderSource{
    wgpu::ShaderSource::Wgsl(Cow::Borrowed(src))
}

