use winit::event_loop::EventLoop;
use std::borrow::Cow;
use winit::window::Window;
use yoi_core::app::{App, Yoi};
use yoi_wgpu::pipeline::{generate_default_pipeline_layout};
use yoi_wgpu::shader::{create_shader_module, generate_wgsl, ShaderDesc};
use yoi_wgpu::webgpuinstance::WebGPUInstance;

struct Model {
    render_pipeline:wgpu::RenderPipeline
}
fn main(){
    App::new(String::from("MultiPass"), 1024, 768)
        .setup(setup)
        .draw(draw)
        .run();
}



fn setup(_app: &Yoi, window: &winit::window::Window,instance:&WebGPUInstance) -> Model {

    // Load the shaders from disk
    let shader = create_shader_module(instance,ShaderDesc{
        label: None,
        source: generate_wgsl(include_str!("../shaders/triangle.wgsl"))
    });

    let swapchain_format = instance.get_default_texture_format();
    let pipeline_layout = generate_default_pipeline_layout(instance);
    let render_pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(swapchain_format.into())],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    });

    Model {
        render_pipeline
    }
}

fn draw(_app: &Yoi, _model: &mut Model,instance:&WebGPUInstance) {

    let device = instance.get_device();
    let surface = instance.get_surface();
    let render_pipeline =&mut _model.render_pipeline;
    let queue = instance.get_queue();

    let frame = surface
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());
    let mut encoder =
        device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
    {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::GREEN),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });
        rpass.set_pipeline(&render_pipeline);
        rpass.draw(0..3, 0..1);
    }

    queue.submit(Some(encoder.finish()));
    frame.present();
}