#[allow(unused,dead_code)]
use winit::event_loop::EventLoop;
use std::borrow::Cow;
use std::ops::Range;
use bytemuck::Pod;
use glam::{Affine3A, Mat4, Quat, Vec3, Vec4};
use wgpu::{ComputePassDescriptor, ComputePipelineDescriptor, Face, include_wgsl, PipelineLayout, ShaderModuleDescriptor};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::window::Window;
use yoi_core::app::{App, MouseState, Yoi};
use yoi_core::math::core::pack_vec4;
use yoi_core::shader_convert::ShaderConvert;
use yoi_core::{get_window_aspect};
use yoi_math::core::{create_vec4, pack_vec3, rand_vec3, rand_vec4};
use yoi_wgpu::bindgroup::{BindGroupEntry, BindGroupLayoutEntry};
use yoi_wgpu::prelude::*;


struct Model {
    orbit_cam:OrbitCam,
    camera:Camera,
    mesh:DisplayObject,
    bind_group:BindGroup,
    compute:ComputeComponent,
    frame_num:usize

}
struct DisplayObject {
    pipeline:wgpu::RenderPipeline,
    vbos:Vec<Vbo>
}


struct ComputeComponent {
    pipeline:wgpu::ComputePipeline,
    bind_groups:Vec<BindGroup>
}

struct Particle {
    position:Vec4,
    velocity:Vec4
}



const NUM_PARTICLES:i32 = 200;

fn main(){
    App::new(String::from("MultiPass"), 1024, 768)
        .setup(setup)
        .mouse_up(on_mouse_up)
        .mouse_down(on_mouse_down)
        .mouse_move(on_mouse_move)
        .draw(draw)
        .run();
}


fn on_mouse_down(app:&Yoi, mouse_state:&mut MouseState,model:&mut Model){

    let mouse_pos = &mouse_state.position;
    let x = &mouse_pos[0];
    let y = &mouse_pos[0];

    model.orbit_cam.on_mouse_down(*x,*y);

}

fn on_mouse_up(app:&Yoi, mouse_state:&mut MouseState,model:&mut Model){
    model.orbit_cam.on_mouse_up();
}

/// handle mouse move events
fn on_mouse_move(app:&Yoi, mouse_state:&mut MouseState,model:&mut Model){


    let mouse_pos = &mouse_state.position;
    let x = &mouse_pos[0];
    let y = &mouse_pos[0];

}
fn setup(_app: &Yoi, window: &winit::window::Window,_instance:&WebGPUInstance) -> Model {



    let mut orbit_cam = OrbitCam::new();
    let mut camera = Camera::new();
    camera.create_perspective(50.0,get_window_aspect(window) as f32,0.1,1000.0);

    /////////// BUILD CAMERA BUFFER /////////////
    let matrices = camera.get_vp().to_cols_array();


    ///////// BUILD BIND GROUP ///////////////
    let mut bind_group = BindGroup::new();
    let camera_buffer = Vbo::new(_instance,BufferInitDescriptor{
        label: Some("camera buffer"),
        contents: bytemuck::cast_slice(&matrices),
        usage: wgpu::BufferUsages::UNIFORM
    });

    // add description of the camera buffer.
    bind_group.add_buffer_layout_entry(&BindGroupEntry{
        binding: 0,
        visibility: wgpu::ShaderStages::VERTEX,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Uniform,
            has_dynamic_offset: false,
            min_binding_size: wgpu::BufferSize::new(64)
        }
    });

    // build the bind group.
    bind_group.build(_instance,&[
        camera_buffer.as_entire_binding()
    ]);

    let mesh = build_mesh(_instance,&bind_group);
    let compute = build_compute(_instance);


    Model {
        orbit_cam,
        camera,
        mesh,
        bind_group,
        compute,
        frame_num:0
    }
}

fn draw(_app: &Yoi, _model: &mut Model,_instance:&WebGPUInstance) {

    let device = _instance.get_device();
    let surface = _instance.get_surface();
    let queue = _instance.get_queue();


    let render_pipeline =&mut _model.mesh.pipeline;
    let vbos = &mut _model.mesh.vbos;
    let bg = &mut _model.bind_group;

    let compute_pipeline = &mut _model.compute.pipeline;
    let c_bindgroups = &mut _model.compute.bind_groups;

    let frame = surface
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());


    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

    // build compute pass
    {
        let mut cpass = encoder.begin_compute_pass(&ComputePassDescriptor{
            label: None
        });

        cpass.set_pipeline(&compute_pipeline);
        cpass.set_bind_group(0,&c_bindgroups[_model.frame_num % 2].get_bindgroup(),&[]);
        cpass.dispatch_workgroups(64,1,1);
    }

    // build render pass
    {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        rpass.set_pipeline(&render_pipeline);
        rpass.set_bind_group(0,bg.get_bindgroup(),&[]);
        for i in 0..vbos.len(){
            let vbo = vbos[i].get_raw_buffer().slice(..);
            rpass.set_vertex_buffer(i as u32,vbo);
        }
        let num = NUM_PARTICLES as u32;
        rpass.draw(0..num, 0..1);
    }

    queue.submit(Some(encoder.finish()));
    frame.present();
    _model.frame_num += 1;
}

/// buidls particles to render.
fn build_mesh(instance:&WebGPUInstance,bind_group:&BindGroup)->DisplayObject{

    let mut verts = vec![0.0f32; (4  * NUM_PARTICLES) as usize];
    let mut vertices = Vbo::new(instance, BufferInitDescriptor {
        label: None,
        contents: bytemuck::cast_slice(&verts),
        usage: wgpu::BufferUsages::VERTEX
    });

    vertices.add_layout_attrib(  wgpu::VertexAttribute {
        format: wgpu::VertexFormat::Float32x4,
        offset: 0,
        shader_location: 0,
    });
    let shader = create_shader_module(instance, ShaderDesc {
        label: None,
        source: generate_wgsl(include_str!("../shaders/particles.wgsl"))
    });

    let pipeline_layout = instance.get_device().create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[bind_group.get_layout()],
        push_constant_ranges: &[]
    });

    let vertex_buffers = [vertices.get_vbo_layout()];

    let swapchain_format = instance.get_default_texture_format();
    let pipeline = instance.get_device().create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &vertex_buffers,
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(swapchain_format.into())],
        }),
        primitive: wgpu::PrimitiveState {
            cull_mode: Some(wgpu::Face::Back),
            topology:wgpu::PrimitiveTopology::PointList,
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    });

    DisplayObject{
        vbos:vec![vertices],
        pipeline
    }
}



/// build the mesh for sketch
fn build_compute(instance:&WebGPUInstance)->ComputeComponent{

    let device = instance.get_device();

    // build particle data
    let mut storage_data = vec![];

    for i in 0..NUM_PARTICLES{

        let pos = create_vec4();
        let vel = rand_vec4();


        storage_data.push(pos);
        storage_data.push(vel)
    }

    let data = pack_vec4(storage_data);


    let buffer_1 = Vbo::new(instance,BufferInitDescriptor{
        label: Some("Particle data buffer 1"),
        contents: bytemuck::cast_slice(&data),
        usage: wgpu::BufferUsages::STORAGE
    });
    let buffer_2 = Vbo::new(instance,BufferInitDescriptor{
        label: Some("Particle data buffer 1"),
        contents: bytemuck::cast_slice(&data.clone()),
        usage: wgpu::BufferUsages::STORAGE
    });


    ///////// BUILD FIRST BIND GROUP //////////////////////

    let mut bg = BindGroup::new();

    bg.add_buffer_layout_entry(&BindGroupEntry{
        binding: 0,
        visibility: wgpu::ShaderStages::COMPUTE,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Storage {read_only:false},
            has_dynamic_offset: false,
            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 32) as u64)
        }
    });


    bg.add_buffer_layout_entry(&BindGroupEntry{
        binding: 1,
        visibility: wgpu::ShaderStages::COMPUTE,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Storage {read_only:false},
            has_dynamic_offset: false,
            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 32) as u64)
        }
    });


    bg.build(instance,&[
        buffer_1.as_entire_binding(),
        buffer_2.as_entire_binding()
    ]);

    ///////// BUILD SECOND BIND GROUP //////////////////////

    let mut bg2 = BindGroup::new();

    bg2.add_buffer_layout_entry(&BindGroupEntry{
        binding: 0,
        visibility: wgpu::ShaderStages::COMPUTE,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Storage {read_only:false},
            has_dynamic_offset: false,
            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 32) as u64)
        }
    });


    bg2.add_buffer_layout_entry(&BindGroupEntry{
        binding: 1,
        visibility: wgpu::ShaderStages::COMPUTE,
        buffer_desc: BindGroupLayoutEntry {
            binding_type: wgpu::BufferBindingType::Storage {read_only:false},
            has_dynamic_offset: false,
            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 32) as u64)
        }
    });


    bg2.build(instance,&[
        buffer_2.as_entire_binding(),
        buffer_1.as_entire_binding()
    ]);




    ////////////// BUILD COMPUTE /////////////////////

    let compute_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: Some("compute"),
        bind_group_layouts: &[bg.get_layout()],
        push_constant_ranges: &[],
    });

    let cs_module = device.create_shader_module(wgpu::ShaderModuleDescriptor {
        label: Some("Test shader"),
        source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("../shaders/swirl_compute.wgsl"))),
    });

    // build compute pipeline
    let compute_pipeline = device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
        label: Some("Particle compute"),
        layout:Some(&compute_pipeline_layout),
        module: &cs_module,
        entry_point: "main",
    });

    let bind_groups = vec![bg,bg2];

    ComputeComponent{
        pipeline:compute_pipeline,
        bind_groups
    }
}