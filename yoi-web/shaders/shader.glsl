
#version 450

in vec2 in_vertex_index;
void main(){
    float x = in_vertex_index.x - 1.0;
    float y = in_vertex_index.y * 2 - 1;

    gl_Position = vec4(x,y,0.0,1.);
}

