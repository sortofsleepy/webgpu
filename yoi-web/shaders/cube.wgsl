
   /////// CAMERA UNIFORMS FOR BLOCK SCENE ////////
       struct Uniforms {
            modelViewProjectionMatrix : mat4x4<f32>;
       };
       [[binding(0), group(0)]] var<uniform> uniforms : Uniforms;


       // vertex output
       struct vertexOutput {
            [[builtin(position)]] Position : vec4<f32>;
       };


       [[stage(vertex)]]
       fn vs_main(
            [[location(0)]] position:vec4<f32>)->vertexOutput {

            //////// DECLARE VARIABLES ////////////
            var output:vertexOutput;

            // re-ref vertex position of cubes.
            var pos:vec4<f32> = position;

            pos.x = pos.x * 2.0;
            pos.y = pos.y * 2.0;
            pos.z = pos.z * 2.0;

            output.Position = uniforms.modelViewProjectionMatrix * vec4<f32>(pos.xyz,1.0);

            return output;
       }

          [[stage(fragment)]]
               fn fs_main() -> [[location(0)]] vec4<f32>{
                   return vec4<f32>(1.0,1.0,0.0,1.0);
               }