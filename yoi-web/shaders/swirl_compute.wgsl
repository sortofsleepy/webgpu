struct Particle {
    position:vec4<f32>;
    velocity:vec4<f32>;
};

struct ParticleData {
    particles:array<Particle>;
};

[[group(0),binding(0)]] var<storage, read_write> particlesSrc : ParticleData;
[[group(0),binding(1)]]  var<storage, read_write> particlesDst : ParticleData;

[[stage(compute), workgroup_size(64)]]
fn main([[builtin(global_invocation_id)]] global_id: vec3<u32>) {

}