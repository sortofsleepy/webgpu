pub mod app;
pub mod file;
use winit::window::Window;

/// returns a window's aspect ratio based on the current size of the window.
pub fn get_window_aspect(win:&Window)->u32{
    let mut size = win.inner_size();
    size.width / size.height
}