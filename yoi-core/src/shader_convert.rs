#![allow(clippy::manual_strip)]
#[allow(unused_imports)]
use std::fs;
use std::{error::Error, fmt, io::Read, path::Path, str::FromStr};


/// Translate shaders to different formats.
#[derive(argh::FromArgs, Debug, Clone)]
struct Args {
    /// bitmask of the ValidationFlags to be used, use 0 to disable validation
    #[argh(option)]
    validate: Option<u8>,

    /// what policy to use for index bounds checking for arrays, vectors, and
    /// matrices.
    ///
    /// May be `Restrict` (force all indices in-bounds), `ReadZeroSkipWrite`
    /// (out-of-bounds indices read zeros, and don't write at all), or
    /// `Unchecked` (generate the simplest code, and whatever happens, happens)
    ///
    /// `Unchecked` is the default.
    #[argh(option)]
    index_bounds_check_policy: Option<BoundsCheckPolicyArg>,

    /// what policy to use for index bounds checking for arrays, vectors, and
    /// matrices, when they are stored in globals in the `storage` or `uniform`
    /// storage classes.
    ///
    /// Possible values are the same as for `index-bounds-check-policy`. If
    /// omitted, defaults to the index bounds check policy.
    #[argh(option)]
    buffer_bounds_check_policy: Option<BoundsCheckPolicyArg>,

    /// what policy to use for texture bounds checking.
    ///
    /// Possible values are the same as for `index-bounds-check-policy`. If
    /// omitted, defaults to the index bounds check policy.
    #[argh(option)]
    image_bounds_check_policy: Option<BoundsCheckPolicyArg>,

    /// directory to dump the SPIR-V block context dump to
    #[argh(option)]
    block_ctx_dir: Option<String>,

    /// the shader entrypoint to use when compiling to GLSL
    #[argh(option)]
    entry_point: Option<String>,

    /// the shader profile to use, for example `es`, `core`, `es330`, if translating to GLSL
    #[argh(option)]
    profile: Option<GlslProfileArg>,

    /// the shader model to use if targeting HSLS
    ///
    /// May be `50`, 51`, or `60`
    #[argh(option)]
    shader_model: Option<ShaderModelArg>,

    /// if the selected frontends/backends support coordinate space conversions,
    /// disable them
    #[argh(switch)]
    keep_coordinate_space: bool,

    /// specify file path to process STDIN as
    #[argh(option)]
    stdin_file_path: Option<String>,

    /// show version
    #[argh(switch)]
    version: bool,

    /// the input and output files.
    ///
    /// First positional argument is the input file. If not specified, the
    /// input will be read from stdin. In the case, --stdin-file-path must also
    /// be specified.
    ///
    /// The rest arguments are the output files. If not specified, only
    /// validation will be performed.
    #[argh(positional)]
    files: Vec<String>,
}

/// Newtype so we can implement [`FromStr`] for `BoundsCheckPolicy`.
#[derive(Debug, Clone, Copy)]
struct BoundsCheckPolicyArg(naga::proc::BoundsCheckPolicy);

impl FromStr for BoundsCheckPolicyArg {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use naga::proc::BoundsCheckPolicy;
        Ok(Self(match s.to_lowercase().as_str() {
            "restrict" => BoundsCheckPolicy::Restrict,
            "readzeroskipwrite" => BoundsCheckPolicy::ReadZeroSkipWrite,
            "unchecked" => BoundsCheckPolicy::Unchecked,
            _ => {
                return Err(format!(
                    "Invalid value for --index-bounds-check-policy: {}",
                    s
                ))
            }
        }))
    }
}

/// Newtype so we can implement [`FromStr`] for [`naga::back::glsl::Version`].
#[derive(Clone, Debug)]
struct GlslProfileArg(naga::back::glsl::Version);

impl FromStr for GlslProfileArg {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use naga::back::glsl::Version;
        Ok(Self(if s.starts_with("core") {
            Version::Desktop(s[4..].parse().unwrap_or(330))
        } else if s.starts_with("es") {
            Version::Embedded(s[2..].parse().unwrap_or(310))
        } else {
            return Err(format!("Unknown profile: {}", s));
        }))
    }
}


/// Newtype so we can implement [`FromStr`] for `ShaderModel`.
#[derive(Debug, Clone)]
struct ShaderModelArg(naga::back::hlsl::ShaderModel);

impl FromStr for ShaderModelArg {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use naga::back::hlsl::ShaderModel;
        Ok(Self(match s.to_lowercase().as_str() {
            "50" => ShaderModel::V5_0,
            "51" => ShaderModel::V5_1,
            "60" => ShaderModel::V6_0,
            _ => return Err(format!("Invalid value for --shader-model: {}", s)),
        }))
    }
}

#[derive(Default)]
struct Parameters {
    validation_flags: naga::valid::ValidationFlags,
    bounds_check_policies: naga::proc::BoundsCheckPolicies,
    entry_point: Option<String>,
    keep_coordinate_space: bool,
    spv_block_ctx_dump_prefix: Option<String>,
    spv: naga::back::spv::Options,
    msl: naga::back::msl::Options,
    glsl: naga::back::glsl::Options,
    hlsl: naga::back::hlsl::Options,
}
/// Error type for the CLI
#[derive(Debug, Clone)]
struct CliError(&'static str);
impl fmt::Display for CliError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)

    }
}
impl std::error::Error for CliError {}
trait PrettyResult {
    type Target;
    fn unwrap_pretty(self) -> Self::Target;
}


impl<T, E: Error> PrettyResult for Result<T, E> {
    type Target = T;
    fn unwrap_pretty(self) -> T {
        match self {
            Result::Ok(value) => value,
            Result::Err(error) => {
                //print_err(&error);
                std::process::exit(1);
            }
        }
    }
}

//////////

pub struct ShaderConvert {}

impl ShaderConvert {
    pub fn glsl_to_wgsl(src:&str)-> Result<String, Box<dyn std::error::Error>>{
        // Initialize default parameters

        let mut params = Parameters::default();

        let v = vec![String::from(src)];

        let (input_path, input) = if let Some(path) = v.first() {
            let path = Path::new(path);
            (path, fs::read(path)?)

        } else {
            return Err(CliError("Input file path is not specified").into());
        };


        let (module, input_text) = match Path::new(&input_path)
            .extension()
            .ok_or(CliError("Input filename has no extension"))?
            .to_str()
            .ok_or(CliError("Input filename not valid unicode"))?
        {

            ext @ "vert" | ext @ "frag" | ext @ "comp" => {
                let input = String::from_utf8(input)?;
                let mut parser = naga::front::glsl::Parser::default();

                (
                    parser
                        .parse(
                            &naga::front::glsl::Options {
                                stage: match ext {
                                    "vert" => naga::ShaderStage::Vertex,
                                    "frag" => naga::ShaderStage::Fragment,
                                    "comp" => naga::ShaderStage::Compute,
                                    _ => unreachable!(),
                                },
                                defines: Default::default(),
                            },
                            &input,
                        )
                        .unwrap_or_else(|errors| {
                            let filename = input_path.file_name().and_then(std::ffi::OsStr::to_str);
                            //emit_glsl_parser_error(errors, filename.unwrap_or("glsl"), &input);
                            std::process::exit(1);
                        }),
                    Some(input),
                )
            }
            _ => return Err(CliError("Unknown input file extension").into()),
        };

        // validate the IR
        let info = match naga::valid::Validator::new(
            params.validation_flags,
            naga::valid::Capabilities::all(),
        )
            .validate(&module)
        {
            Ok(info) => Some(info),
            Err(error) => {
                if let Some(input) = input_text {
                    let filename = input_path.file_name().and_then(std::ffi::OsStr::to_str);
                    //emit_annotated_error(&error, filename.unwrap_or("input"), &input);
                }
                //print_err(&error);
                None
            }
        };


        use naga::back::wgsl;

        let wgsl = wgsl::write_string(
            &module,
            info.as_ref().ok_or(CliError(
                "Generating wgsl output requires validation to \
                        succeed, and it failed in a previous step",
            ))?,
            wgsl::WriterFlags::empty(),
        )
            .unwrap_pretty();

        println!("{}",wgsl);

        Ok(wgsl)
    }

}