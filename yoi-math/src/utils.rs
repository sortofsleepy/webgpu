
use std::any::TypeId;
use glam::{Vec2, Vec3, Vec4};

/// Checks to see if value is a
#[allow(unused)]
pub fn is_vec4<T:'static>(val: &T) -> bool {
    return if TypeId::of::<T>() == TypeId::of::<Vec4>() {
        true
    } else {
        false
    }
}
#[allow(unused)]
pub fn is_vec3<T:'static>(val: &T) -> bool {
    return if TypeId::of::<T>() == TypeId::of::<Vec3>() {
        true
    } else {
        false
    }
}
#[allow(unused)]
pub fn is_vec2<T:'static>(val: &T) -> bool {
    return if TypeId::of::<T>() == TypeId::of::<Vec2>() {
        true
    } else {
        false
    }
}