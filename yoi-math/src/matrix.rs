use glam::{Mat4, Vec3, Vec3A};

pub trait Matrix {
    /// translates matrix.
    fn translate(&mut self, vec: Vec3A);

    /// rotates matrix.
    fn rotate(&mut self, rad: f32, axis: [f32; 3]);

    fn identity(&mut self);

    fn get_raw(&self) -> Mat4;
}

/// wrapper around Glam Mat4
#[derive(Copy, Clone)]
pub struct Matrix4 {
    matrix: Mat4,
}

impl Matrix4 {
    pub fn new() -> Matrix4 {
        Matrix4 {
            matrix: Mat4::IDENTITY
        }
    }
    pub fn IDENTITY() -> Matrix4 {
        Matrix4::new()
    }

    pub fn invert(mut self) -> Self {
        self.matrix = self.matrix.inverse();
        self
    }
}

impl Matrix for Matrix4 {
    fn identity(&mut self) {
        self.matrix = Mat4::IDENTITY;
    }

    fn get_raw(&self) -> Mat4 {
        self.matrix
    }

    /// translates the matrix.
    fn translate(&mut self, vec: Vec3A) {

        // x is reversed for some reason, flip it to follow OpenGL conventions.
        let mut v = vec;
        v.x = v.x * -1.0;

        self.matrix *= Mat4::from_translation(v.into());
    }

    /// rotates the matrix. ported from glMatrix.
    fn rotate(&mut self, rad: f32, axis: [f32; 3]) {
        let m = Mat4::from_axis_angle(Vec3::new(axis[0], axis[1], axis[2]), rad);
        self.matrix *= m;

        /*
          let mut x = axis[0];
        let mut y = axis[1];
        let mut z = axis[2];

        let mat = self.matrix;

        let tmp_len = ((x * x) + (y * y) + (z * z));
        let mut len = (tmp_len as f32).sqrt();
        len = 1.0 / len;


        x *= len;
        y *= len;
        z *= len;

        let s = rad.sin();
        let c = rad.cos();
        let t = 1.0 - c;

        let a00 = mat.x_axis.x;
        let a01 = mat.x_axis.y;
        let a02 = mat.x_axis.z;
        let a03 = mat.x_axis.w;

        let a10 = mat.y_axis.x;
        let a11 = mat.y_axis.y;
        let a12 = mat.y_axis.z;
        let a13 = mat.y_axis.w;

        let a10 = mat.z_axis.x;
        let a11 = mat.z_axis.y;
        let a12 = mat.z_axis.z;
        let a13 = mat.z_axis.w;

        let b00 = x * x * t + c;
        let b01 = y * x * t + z * s;
        let b02 = z * x * t - y * s;
        let b10 = x * y * t - z * s;
        let b11 = y * y * t + c;
        let b12 = z * y * t + x * s;
        let b20 = x * z * t + y * s;
        let b21 = y * z * t - x * s;
        let b22 = z * z * t + c;

        let row_x = self.matrix.col_mut(0);
        row_x[0] = a00 * b00 + a10 * b01 + a20 * b02;
        row_x[1] = a01 * b00 + a11 * b01 + a21 * b02;
        row_x[2] = a02 * b00 + a12 * b01 + a22 * b02;
        row_x[3] = a03 * b00 + a13 * b01 + a23 * b02;

        let row_y = self.matrix.col_mut(1);
        row_y[4] = a00 * b10 + a10 * b11 + a20 * b12;
        row_y[5] = a01 * b10 + a11 * b11 + a21 * b12;
        row_y[6] = a02 * b10 + a12 * b11 + a22 * b12;
        row_y[7] = a03 * b10 + a13 * b11 + a23 * b12;

        let row_z = self.matrix.col_mut(2);
        row_z[8] = a00 * b20 + a10 * b21 + a20 * b22;
        row_z[9] = a01 * b20 + a11 * b21 + a21 * b22;
        row_z[10] = a02 * b20 + a12 * b21 + a22 * b22;
        row_z[11] = a03 * b20 + a13 * b21 + a23 * b22;
         */
    }
}

