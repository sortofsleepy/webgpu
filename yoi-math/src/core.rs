use glam::{Vec3, Vec4};
use rand::Rng;

/// Unrolls a Vector of Vec4s into
pub fn pack_vec4(_vec:Vec<Vec4>) -> Vec<[f32;4]> {
    let mut bytes = vec![];
    for v in _vec {
        bytes.push(v.to_array())
    }

    bytes
}

/// pack vec3 into an array
pub fn pack_vec3(_vec:Vec<Vec3>) -> Vec<[f32;3]> {
    let mut bytes = vec![];
    for v in _vec {
        bytes.push(v.to_array())
    }

    bytes
}

/// helper to quickly init a vec 3
pub fn create_vec3()->Vec3{
    Vec3::new(0.0,0.0,0.0)
}

/// helper to quickly init a vec 4
pub fn create_vec4()->Vec4{ Vec4::new(0.0,0.0,0.0,1.0) }

/// Linear interpolates a value.
pub fn lerp(val:f32, min:f32, max:f32)->f32{
    min + (max - min) * val
}

/// returns a random f32 value between a min and max.
pub fn rand_float(min:f32, max:f32)->f32{
    let mut rng = rand::thread_rng();
    let val:f32 = rng.gen();
    val * (max - min) + min
}

/// returns a random vec3
pub fn rand_vec3()->Vec3{
    Vec3::new(rand_float(0.0,1.0),rand_float(0.0,1.0),rand_float(0.0,1.0))
}

/// returns a random vec4
pub fn rand_vec4()->Vec4{
    Vec4::new(rand_float(-1.0,1.0),rand_float(-1.0,1.0),rand_float(-1.0,1.0),1.0)
}
